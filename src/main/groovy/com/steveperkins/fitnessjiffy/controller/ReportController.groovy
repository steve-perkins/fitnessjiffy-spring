package com.steveperkins.fitnessjiffy.controller

import com.steveperkins.fitnessjiffy.domain.ReportData
import com.steveperkins.fitnessjiffy.domain.User
import com.steveperkins.fitnessjiffy.service.ReportDataService
import com.steveperkins.fitnessjiffy.service.UserService
import groovy.transform.AutoFinal
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.server.ResponseStatusException

import javax.servlet.http.HttpServletRequest
import java.time.LocalDate
import java.time.format.DateTimeParseException

@RestController
@RequestMapping('/api/report')
@AutoFinal
@CompileStatic
class ReportController {

    private ReportDataService reportDataService
    private UserService userService

    @Autowired
    ReportController(ReportDataService reportDataService, UserService userService) {
        this.reportDataService = reportDataService
        this.userService = userService
    }

    @GetMapping
    List<ReportData> reportData(
            @RequestParam(name = 'startDate', required = false) String startDateStr,
            HttpServletRequest request
    ) {
        User user = userService.currentAuthenticatedUser(request)
        LocalDate startDate = null
        if (startDateStr != null) {
            try {
                startDate = LocalDate.parse(startDateStr)
            } catch (DateTimeParseException ignored) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "'startDate' cannot be parsed")
            }
        }

        reportDataService.findByUser(user.id, startDate)
    }

}
