package com.steveperkins.fitnessjiffy.controller

import com.steveperkins.fitnessjiffy.domain.User
import com.steveperkins.fitnessjiffy.domain.Weight
import com.steveperkins.fitnessjiffy.service.UserService
import com.steveperkins.fitnessjiffy.service.WeightService
import groovy.transform.AutoFinal
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.server.ResponseStatusException

import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import java.time.LocalDate

@RestController
@RequestMapping('/api/weight')
@AutoFinal
@CompileStatic
class WeightController {

    private UserService userService
    private WeightService weightService

    @Autowired
    WeightController(UserService userService, WeightService weightService) {
        this.userService = userService
        this.weightService = weightService
    }

    @GetMapping('/{date}')
    double loadWeight(
            @PathVariable(name = 'date', required = false) String dateString,
            HttpServletRequest request
    ) {
        User user = userService.currentAuthenticatedUser(request)
        LocalDate date = (dateString == null || dateString.isEmpty())
                ? LocalDate.now(user.timeZone)
                : LocalDate.parse(dateString)
        Weight weight = weightService.findWeightOnDate(user.id, date)
        if (weight == null) throw new ResponseStatusException(HttpStatus.NOT_FOUND)
        weight.getPounds()
    }

    @PostMapping('/{date}')
    void insertWeight(
            @PathVariable(name = 'date', required = false) String dateString,
            @RequestBody Weight weight,
            HttpServletRequest request,
            HttpServletResponse response
    ) {
        upsertWeight(dateString, weight, request, response)
    }

    @PutMapping('/{date}')
    void updateWeight(
            @PathVariable(name = 'date', required = false) String dateString,
            @RequestBody Weight weight,
            HttpServletRequest request,
            HttpServletResponse response
    ) {
        upsertWeight(dateString, weight, request, response)
    }

    private void upsertWeight(String dateString, Weight weight, HttpServletRequest request, HttpServletResponse response) {
        if (weight.pounds == 0) {
            response.status = HttpStatus.BAD_REQUEST.value()
            return
        }
        User user = userService.currentAuthenticatedUser(request)
        weight.userId = user.id
        weight.date = dateString == null ? LocalDate.now(user.timeZone) : LocalDate.parse(dateString)
        weightService.upsert(weight)
    }

}
