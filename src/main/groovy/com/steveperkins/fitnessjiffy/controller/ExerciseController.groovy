package com.steveperkins.fitnessjiffy.controller

import com.steveperkins.fitnessjiffy.domain.Exercise
import com.steveperkins.fitnessjiffy.domain.ExercisePerformed
import com.steveperkins.fitnessjiffy.domain.User
import com.steveperkins.fitnessjiffy.service.ExerciseService
import com.steveperkins.fitnessjiffy.service.UserService
import groovy.transform.AutoFinal
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.server.ResponseStatusException

import javax.servlet.http.HttpServletRequest
import java.time.LocalDate
import java.time.format.DateTimeParseException

@RestController
@RequestMapping('/api/exercise')
@AutoFinal
@CompileStatic
class ExerciseController {

    private ExerciseService exerciseService
    private UserService userService

    @Autowired
    ExerciseController(ExerciseService exerciseService, UserService userService) {
        this.exerciseService = exerciseService
        this.userService = userService
    }

    //
    // Exercise
    //

    @GetMapping('/{partialDescription}')
    List<Exercise> findByDescriptionLike(@PathVariable(name = 'partialDescription', required = true) String partialDescription) {
        List<Exercise> exercises = exerciseService.findByDescriptionLike(partialDescription)
        if (exercises.isEmpty()) throw new ResponseStatusException(HttpStatus.NOT_FOUND)
        exercises
    }

    @GetMapping('/category')
    List<String> exerciseCategories() {
        exerciseService.findAllCategories()
    }

    @GetMapping('/category/{category}')
    List<Exercise> exercisesInCategory(@PathVariable(name = 'category', required = true) String category) {
        List<Exercise> exercises = exerciseService.findByCategory(category)
        if (exercises.isEmpty()) throw new ResponseStatusException(HttpStatus.NOT_FOUND)
        exercises
    }

    @GetMapping('/recent/{fromDate}')
    List<Exercise> recentExercises(
            @PathVariable(name = 'fromDate', required = true) String fromDateString,
            HttpServletRequest request
    ) {
        LocalDate fromDate
        try {
            fromDate = LocalDate.parse(fromDateString)
        } catch (DateTimeParseException ignored) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "'fromDate' cannot be parsed")
        }

        User user = userService.currentAuthenticatedUser(request)
        List<Exercise> exercises = exerciseService.findByUserPerformedWithinRange(user.id, fromDate, LocalDate.now())

        if (exercises.isEmpty()) throw new ResponseStatusException(HttpStatus.NOT_FOUND)
        exercises
    }

    //
    // Exercise Performed
    //

    @GetMapping(['/performed', '/performed/{date}'])
    List<ExercisePerformed> exercisesPerformed(
            @PathVariable(name = 'date', required = false) String dateString,
            HttpServletRequest request
    ) {
        User user = userService.currentAuthenticatedUser(request)
        LocalDate date
        if (dateString == null) {
            date = LocalDate.now(user.timeZone)
        } else {
            try {
                date = LocalDate.parse(dateString)
            } catch (DateTimeParseException ignored) {
                throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "'date' cannot be parsed")
            }
        }

        List<ExercisePerformed> exercisesPerformed = exerciseService.findByUserOnDate(user.id, date)
        if (exercisesPerformed.isEmpty()) throw new ResponseStatusException(HttpStatus.NOT_FOUND)
        exercisesPerformed
    }

    @PostMapping('/performed')
    ExercisePerformed createExercisePerformed(
            @RequestBody(required = true) ExercisePerformed exercisePerformed,
            HttpServletRequest request
    ) {
        exercisePerformed.id = exercisePerformed.id ?: UUID.randomUUID()
        doUpsert(exercisePerformed, request)
        exercisePerformed
    }

    @PutMapping('/performed')
    void updateExercisePerformed(
            @RequestBody(required = true) ExercisePerformed exercisePerformed,
            HttpServletRequest request
    ) {
        doUpsert(exercisePerformed, request)
    }

    @DeleteMapping('/performed/{id}')
    void deleteExercisePerformed(@PathVariable(name = 'id', required = true) UUID id) {
        exerciseService.deletePerformed(id)
    }

    private void doUpsert(ExercisePerformed exercisePerformed, HttpServletRequest request) {
        User user = userService.currentAuthenticatedUser(request)
        if (exercisePerformed.userId != null && exercisePerformed.userId != user.id) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, 'Cannot store an exercise performed record belonging to another user')
        } else {
            exercisePerformed.userId = user.id
        }
        exerciseService.upsertPerformed(exercisePerformed)
    }

}
