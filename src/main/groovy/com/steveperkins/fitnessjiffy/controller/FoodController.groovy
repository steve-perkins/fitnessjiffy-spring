package com.steveperkins.fitnessjiffy.controller

import com.steveperkins.fitnessjiffy.domain.Food
import com.steveperkins.fitnessjiffy.domain.FoodEaten
import com.steveperkins.fitnessjiffy.domain.User
import com.steveperkins.fitnessjiffy.service.FoodService
import com.steveperkins.fitnessjiffy.service.UserService
import groovy.transform.AutoFinal
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.DeleteMapping
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.server.ResponseStatusException

import javax.servlet.http.HttpServletRequest
import java.time.LocalDate
import java.time.format.DateTimeParseException

@RestController
@RequestMapping('/api/food')
@AutoFinal
@CompileStatic
class FoodController {

    private FoodService foodService
    private UserService userService

    @Autowired
    FoodController(FoodService foodService, UserService userService) {
        this.foodService = foodService
        this.userService = userService
    }

    //
    // Food
    //

    @GetMapping('/{partialName}')
    List<Food> searchFoods(
            @PathVariable(name = 'partialName', required = true) String partialName,
            HttpServletRequest request
    ) {
        User user = userService.currentAuthenticatedUser(request)
        List<Food> foods = foodService.searchFoods(user.id, partialName)
        if (foods.isEmpty()) throw new ResponseStatusException(HttpStatus.NOT_FOUND)
        foods
    }

    @GetMapping('/recent/{startDateString}')
    List<Food> recentFoods(@PathVariable(name = 'startDateString', required = true) String startDateString, HttpServletRequest request) {
        LocalDate startDate
        try {
            startDate = LocalDate.parse(startDateString)
        } catch (DateTimeParseException ignored) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "'startDate' cannot be parsed")
        }
        User user = userService.currentAuthenticatedUser(request)
        foodService.findByUserPerformedWithinRange(user.id, startDate, LocalDate.now(user.timeZone))
    }

    @PostMapping
    Food createFood(@RequestBody(required = true) Food food, HttpServletRequest request) {
        upsertFood(food, request)
        food
    }

    @PutMapping
    void updateFood(@RequestBody(required = true) Food food, HttpServletRequest request) {
        upsertFood(food, request)
    }

    private void upsertFood(Food food, HttpServletRequest request) {
        User user = userService.currentAuthenticatedUser(request)
        if (food.ownerId != null && food.ownerId != user.id) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, 'Cannot store a food belonging to another user')
        } else if (food.ownerId == null) {
            // Creating a copy of a global food.  Ensure that it has a unique ID (we can clobber any existing value
            // because the UI client isn't supposed to be generating those anyway).
            food.id = UUID.randomUUID()
            food.ownerId = user.id
        }
        foodService.upsertFood(food)
    }

    //
    // Food Eaten
    //

    @GetMapping('/eaten/{date}')
    List<FoodEaten> findFoodsEaten(@PathVariable(name = 'date', required = true) String dateString, HttpServletRequest request) {
        LocalDate date
        try {
            date = LocalDate.parse(dateString)
        } catch (DateTimeParseException ignored) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "'date' cannot be parsed")
        }

        User user = userService.currentAuthenticatedUser(request)
        List<FoodEaten> foodsEaten = foodService.findEaten(user.id, date)
        if (foodsEaten.isEmpty()) throw new ResponseStatusException(HttpStatus.NOT_FOUND)
        foodsEaten
    }

    @PostMapping('/eaten')
    FoodEaten createFoodEaten(@RequestBody(required = true) FoodEaten foodEaten, HttpServletRequest request) {
        foodEaten.id = foodEaten.id ?: UUID.randomUUID()
        upsertFoodEaten(foodEaten, request)
        foodEaten
    }

    @PutMapping('/eaten')
    void updateFoodEaten(@RequestBody(required = true) FoodEaten foodEaten, HttpServletRequest request) {
        upsertFoodEaten(foodEaten, request)
    }

    @DeleteMapping('/eaten/{id}')
    void deleteFoodEaten(@PathVariable(name = 'id', required = true) UUID id) {
        foodService.deleteFoodEaten(id)
    }

    void upsertFoodEaten(FoodEaten foodEaten, HttpServletRequest request) {
        User user = userService.currentAuthenticatedUser(request)
        if (foodEaten.userId != null && foodEaten.userId != user.id) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, 'Cannot store a food eaten record belonging to another user')
        } else {
            foodEaten.userId = user.id
        }
        foodService.upsertFoodEaten(foodEaten)
    }

}
