package com.steveperkins.fitnessjiffy.controller

import com.steveperkins.fitnessjiffy.exception.AuthenticationException
import com.steveperkins.fitnessjiffy.domain.User
import com.steveperkins.fitnessjiffy.service.UserService
import groovy.transform.AutoFinal
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.PutMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import java.time.LocalDate
import java.time.ZoneId

@RestController
@RequestMapping('/api/user')
@AutoFinal
@CompileStatic
class UserController {

    private UserService userService

    @Autowired
    UserController(UserService userService) {
        this.userService = userService
    }

    @GetMapping
    User loadUser(HttpServletRequest request) {
        userService.currentAuthenticatedUser(request)
    }

    @PostMapping
    void createUser(@RequestBody Map payload, HttpServletResponse response) {
        if (payload?.email == null || payload?.password == null) {
            response.status = HttpStatus.BAD_REQUEST.value()
            return
        }
        userService.createUser(payload.email as String, payload.password as String)
    }

    @PutMapping
    void updateUser(@RequestBody Map payload, HttpServletRequest request, HttpServletResponse response) {
        if (payload == null) {
            response.status = HttpStatus.BAD_REQUEST.value()
            return
        }
        User user = userService.currentAuthenticatedUser(request)

        userService.updateUser(
                user,
                User.Gender.fromString(payload.gender as String),
                LocalDate.parse(payload.birthdate as String),
                payload.heightInInches as Double,
                User.ActivityLevel.fromString(payload.activityLevel as String),
                payload.email as String,
                payload.firstName as String,
                payload.lastName as String,
                ZoneId.of(payload.timeZone as String)
        )
    }

    @PutMapping('/password')
    Map changePassword(@RequestBody Map payload, HttpServletRequest request, HttpServletResponse response) {
        if (payload?.currentPassword == null || payload?.newPassword == null || payload?.confirmNewPassword == null) {
            response.status = HttpStatus.BAD_REQUEST.value()
            return [error: 'Current password, new password, and confirmed new password are required fields']
        }
        try {
            User user = userService.currentAuthenticatedUser(request)
            userService.changePassword(user, payload.currentPassword as String, payload.newPassword as String, payload.confirmNewPassword as String)
            return [token: userService.buildJWT(user.email)]
        } catch (AuthenticationException e) {
            response.status = HttpStatus.BAD_REQUEST.value()
            return [error: e.message]
        }
    }

    @PostMapping('/auth')
    Map passwordAuthentication(@RequestBody Map payload, HttpServletResponse response) {
        if (payload?.email == null || payload?.password == null) {
            response.status = HttpStatus.BAD_REQUEST.value()
            return [error: 'Email and password are required']
        }
        if (!userService.verifyPassword(payload.email as String, payload.password as String)) {
            response.status = HttpStatus.UNAUTHORIZED.value()
            return [error: 'The username and password are not valid']
        }

        [token: userService.buildJWT(payload.email as String)]
    }

}
