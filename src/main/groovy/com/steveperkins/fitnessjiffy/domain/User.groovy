package com.steveperkins.fitnessjiffy.domain

import com.fasterxml.jackson.annotation.JsonIgnore
import groovy.transform.AutoFinal
import groovy.transform.Canonical
import groovy.transform.CompileStatic

import java.sql.Timestamp
import java.time.LocalDate
import java.time.ZoneId

@Canonical
@AutoFinal
@CompileStatic
class User {

    @JsonIgnore UUID id = UUID.randomUUID()
    Gender gender = Gender.FEMALE
    LocalDate birthdate = LocalDate.now()
    double heightInInches = 60
    ActivityLevel activityLevel = ActivityLevel.SEDENTARY
    String email
    String passwordHash
    String firstName = 'First Name'
    String lastName = 'Last Name'
    ZoneId timeZone = ZoneId.of('America/New_York')
    @JsonIgnore Timestamp createdTime = new Timestamp(System.currentTimeMillis())
    @JsonIgnore Timestamp lastUpdatedTime = new Timestamp(System.currentTimeMillis())
    @JsonIgnore LocalDate reportsDirtyDate = LocalDate.now()

    enum Gender {
        MALE, FEMALE

        static Gender fromString(String s) {
            values().find { it.toString().equalsIgnoreCase(s) }
        }
    }

    enum ActivityLevel {
        SEDENTARY(1.25 as double), LIGHTLY_ACTIVE(1.3 as double), MODERATELY_ACTIVE(1.5 as double), VERY_ACTIVE(1.7 as double), EXTREMELY_ACTIVE(2.0 as double)
        private double value
        ActivityLevel(double value) {
            this.value = value
        }

        static ActivityLevel fromString(String s) {
            values().find { it.toString().equalsIgnoreCase(s) }
        }

        static ActivityLevel fromValue(double v) {
            values().find { it.value == v }
        }

        double getValue() {
            this.value
        }

        @Override
        String toString() {
            super.toString().toLowerCase().tokenize('_').collect { it.capitalize() }.join(' ')
        }
    }

}
