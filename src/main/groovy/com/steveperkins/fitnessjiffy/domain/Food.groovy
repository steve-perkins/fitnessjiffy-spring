package com.steveperkins.fitnessjiffy.domain

import com.fasterxml.jackson.annotation.JsonIgnore
import groovy.transform.AutoFinal
import groovy.transform.Canonical
import groovy.transform.CompileStatic

import java.sql.Timestamp

@Canonical
@AutoFinal
@CompileStatic
class Food {

    UUID id = UUID.randomUUID()
    UUID ownerId
    String name
    ServingType defaultServingType
    double servingTypeQty
    int calories
    double fat
    double saturatedFat
    double carbs
    double fiber
    double sugar
    double protein
    double sodium
    @JsonIgnore Timestamp createdTime = new Timestamp(System.currentTimeMillis())
    @JsonIgnore Timestamp lastUpdatedTime = new Timestamp(System.currentTimeMillis())

    enum ServingType {
        OUNCE(1), CUP(8), POUND(16), PINT(16), TABLESPOON(0.5 as double), TEASPOON(0.1667 as double), GRAM(0.03527 as double), CUSTOM(0);
        private double value
        ServingType(double value) {
            this.value = value
        }

        static ServingType fromString(String s) {
            values().find { it.toString().equalsIgnoreCase(s) }
        }
        double getValue() {
            this.value
        }
    }

}
