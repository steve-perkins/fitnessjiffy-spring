package com.steveperkins.fitnessjiffy.domain

import groovy.transform.AutoFinal
import groovy.transform.Canonical
import groovy.transform.CompileStatic

@Canonical
@AutoFinal
@CompileStatic
class Exercise {

    UUID id = UUID.randomUUID()
    String code
    double metabolicEquivalent
    String category
    String description

}
