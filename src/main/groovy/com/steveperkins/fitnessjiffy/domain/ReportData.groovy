package com.steveperkins.fitnessjiffy.domain

import groovy.transform.AutoFinal
import groovy.transform.Canonical
import groovy.transform.CompileStatic

import java.time.LocalDate

@Canonical
@AutoFinal
@CompileStatic
class ReportData {

    private UUID id
    private UUID userId
    private LocalDate date
    private double pounds
    private int netCalories

}
