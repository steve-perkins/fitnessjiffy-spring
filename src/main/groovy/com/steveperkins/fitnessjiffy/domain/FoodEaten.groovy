package com.steveperkins.fitnessjiffy.domain

import groovy.transform.AutoFinal
import groovy.transform.Canonical
import groovy.transform.CompileStatic

import java.time.LocalDate

@Canonical
@AutoFinal
@CompileStatic
class FoodEaten {

    //
    // Actual database columns
    //
    UUID id = UUID.randomUUID()
    UUID userId
    UUID foodId
    LocalDate date
    Food.ServingType servingType
    double servingQty

    //
    // Additional read-only calculated fields
    //
    String name
    int calories
    double fat
    double saturatedFat
    double carbs
    double fiber
    double sugar
    double protein
    double sodium
}
