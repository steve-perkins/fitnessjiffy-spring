package com.steveperkins.fitnessjiffy.domain

import com.fasterxml.jackson.annotation.JsonIgnore
import groovy.transform.Canonical
import groovy.transform.CompileStatic

import java.time.LocalDate

@Canonical
@CompileStatic
class Weight {

    @JsonIgnore UUID id = UUID.randomUUID()
    @JsonIgnore UUID userId
    LocalDate date
    double pounds

}
