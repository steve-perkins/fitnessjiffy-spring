package com.steveperkins.fitnessjiffy.domain

import groovy.transform.AutoFinal
import groovy.transform.Canonical
import groovy.transform.CompileStatic

import java.time.LocalDate

@Canonical
@AutoFinal
@CompileStatic
class ExercisePerformed {

    UUID id = UUID.randomUUID()
    UUID userId
    UUID exerciseId
    LocalDate date
    int minutes

    int calories
    String description

}
