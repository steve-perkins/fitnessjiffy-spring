package com.steveperkins.fitnessjiffy.service

import com.steveperkins.fitnessjiffy.dao.FoodDAO
import com.steveperkins.fitnessjiffy.domain.Food
import com.steveperkins.fitnessjiffy.domain.Food.ServingType
import com.steveperkins.fitnessjiffy.domain.FoodEaten
import groovy.transform.AutoFinal
import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

import java.time.LocalDate

@Service
@AutoFinal
@CompileStatic
@Slf4j('LOGGER')
class FoodService {

    private UserService userService
    private FoodDAO foodDAO

    @Autowired
    FoodService(UserService userService, FoodDAO foodDAO) {
        this.userService = userService
        this.foodDAO = foodDAO
    }

    //
    // Food
    //

    List<Food> searchFoods(UUID userId, String partialName) {
        foodDAO.searchFoods(userId, partialName)
    }

    List<Food> findByUserPerformedWithinRange(UUID userId, LocalDate startDate, LocalDate endDate) {
        foodDAO.findByUserPerformedWithinRange(userId, startDate, endDate)
    }

    void upsertFood(Food food) {
        foodDAO.upsertFood(food)
        Optional<FoodEaten> earliestEaten = foodDAO.findEarliestFoodEaten(food.ownerId, food.id)
        if (earliestEaten.isPresent()) {
            userService.updateReportsDirtyDate(food.ownerId, earliestEaten.get().date)
        }
    }

    //
    // Food Eaten
    //

    List<FoodEaten> findEaten(UUID userId, LocalDate date) {
        foodDAO.findEatenOnDate(userId, date).collect { foodEaten ->
            // Enrich each "FoodEaten" record with calculated field
            Optional<Food> food = foodDAO.findFoodById(foodEaten.foodId)
            if (!food.isPresent()) {
                LOGGER.error("Food eaten [$foodEaten.id] references non-existent food [$foodEaten.foodId].  Skipping.")
                return null
            }

            double ratio = getRatioToDefaults(foodEaten.servingQty, foodEaten.servingType, food.get().servingTypeQty, food.get().defaultServingType)
            foodEaten.calories = (int) (food.get().calories * ratio)
            foodEaten.fat = food.get().fat * ratio
            foodEaten.saturatedFat = food.get().saturatedFat * ratio
            foodEaten.carbs = food.get().carbs * ratio
            foodEaten.fiber = food.get().fiber * ratio
            foodEaten.sugar = food.get().sugar * ratio
            foodEaten.protein = food.get().protein * ratio
            foodEaten.sodium = food.get().sodium * ratio

            foodEaten
        }.findAll { it != null }
    }

    void upsertFoodEaten(FoodEaten foodEaten) {
        foodDAO.upsertFoodEaten(foodEaten)
        userService.updateReportsDirtyDate(foodEaten.userId, foodEaten.date)
    }

    void deleteFoodEaten(UUID id) {
        FoodEaten foodEaten = foodDAO.findEatenById(id).get()
        foodDAO.deleteFoodEaten(id)
        userService.updateReportsDirtyDate(foodEaten.userId, foodEaten.date)
    }

    private static double getRatioToDefaults(
            double servingQty,
            ServingType servingType,
            double defaultServingQty,
            ServingType defaultServingType
    ) {
        servingType == defaultServingType
                ? servingQty / defaultServingQty
                : (servingType.value * servingQty) / (defaultServingType.value * defaultServingQty)
    }

}
