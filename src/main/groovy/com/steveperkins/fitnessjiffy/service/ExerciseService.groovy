package com.steveperkins.fitnessjiffy.service

import com.steveperkins.fitnessjiffy.dao.ExerciseDAO
import com.steveperkins.fitnessjiffy.dao.WeightDAO
import com.steveperkins.fitnessjiffy.domain.Exercise
import com.steveperkins.fitnessjiffy.domain.ExercisePerformed
import com.steveperkins.fitnessjiffy.domain.Weight
import groovy.transform.AutoFinal
import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

import java.time.LocalDate

@Service
@AutoFinal
@CompileStatic
@Slf4j('LOGGER')
class ExerciseService {

    private UserService userService
    private ExerciseDAO exerciseDAO
    private WeightDAO weightDAO

    @Autowired
    ExerciseService(UserService userService, ExerciseDAO exerciseDAO, WeightDAO weightDAO) {
        this.userService = userService
        this.exerciseDAO = exerciseDAO
        this.weightDAO = weightDAO
    }

    //
    // Exercise
    //

    List<Exercise> findByDescriptionLike(String partialDescription) {
        exerciseDAO.findByDescriptionLike(partialDescription)
    }

    List<String> findAllCategories() {
        exerciseDAO.findAllCategories()
    }

    List<Exercise> findByCategory(String category) {
        exerciseDAO.findByCategory(category)
    }

    List<Exercise> findByUserPerformedWithinRange(UUID userId, LocalDate startDate, LocalDate endDate) {
        exerciseDAO.findByUserPerformedWithinRange(userId, startDate, endDate)
    }


    //
    // Exercise Performed
    //

    List<ExercisePerformed> findByUserOnDate(UUID userId, LocalDate date) {
        exerciseDAO.findPerformedByUserAndDate(userId, date).collect { exercisePerformed ->
            Optional<Exercise> exercise = exerciseDAO.findById(exercisePerformed.exerciseId)
            if (!exercise.isPresent()) {
                LOGGER.error("Exercise performed [${exercisePerformed.id}] is linked to non-existent exercise [${exercisePerformed.exerciseId}].  Skipping.")
                return null
            }
            Weight weight = weightDAO.findByUserMostRecentOnDate(userId, date)
            if (weight == null) {
                LOGGER.error("User [$userId] has no weight records as of date [$date].  Cannot make calorie calculates necessary to load an exercise performed record.")
                return null
            }

            exercisePerformed.description = exercise.get().description
            exercisePerformed.calories = calculateCaloriesBurned(exercise.get().metabolicEquivalent, exercisePerformed.minutes, weight.pounds)
            exercisePerformed
        }.findAll { it != null }
    }

    void upsertPerformed(ExercisePerformed exercisePerformed) {
        exerciseDAO.upsertPerformed(exercisePerformed)
        userService.updateReportsDirtyDate(exercisePerformed.userId, exercisePerformed.date)
    }

    void deletePerformed(UUID id) {
        ExercisePerformed exercisePerformed = exerciseDAO.findPerformedById(id).get()
        exerciseDAO.deletePerformed(id)
        userService.updateReportsDirtyDate(exercisePerformed.userId, exercisePerformed.date)
    }

    private static int calculateCaloriesBurned(double metabolicEquivalent, int minutes, double weightInPounds) {
        double weightInKilograms = weightInPounds / 2.2
        (metabolicEquivalent * 3.5 * weightInKilograms / 200 * minutes) as int
    }

}
