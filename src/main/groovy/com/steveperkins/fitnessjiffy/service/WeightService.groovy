package com.steveperkins.fitnessjiffy.service

import com.steveperkins.fitnessjiffy.dao.WeightDAO
import com.steveperkins.fitnessjiffy.domain.Weight
import groovy.transform.AutoFinal
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

import java.time.LocalDate

@Service
@AutoFinal
@CompileStatic
class WeightService {

    private UserService userService
    private WeightDAO weightDAO

    @Autowired
    WeightService(UserService userService, WeightDAO weightDAO) {
        this.userService = userService
        this.weightDAO = weightDAO
    }

    /**
     * <p>Returns the user's Weight record for the given date, if one exists.</p>
     *
     * <p>Otherwise, returns the user's prior Weight record that is closest to the given date.</p>
     *
     * <p>Returns <code>null</code> only if the user has no Weight records at all.</p>
     */
    Weight findWeightOnDate(UUID userId, LocalDate date) {
        weightDAO.findByUserMostRecentOnDate(userId, date)
    }

    void upsert(Weight weight) {
        weightDAO.upsert(weight)
        userService.updateReportsDirtyDate(weight.userId, weight.date)
    }

}
