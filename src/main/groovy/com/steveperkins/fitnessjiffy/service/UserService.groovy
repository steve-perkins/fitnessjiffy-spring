package com.steveperkins.fitnessjiffy.service

import com.steveperkins.fitnessjiffy.dao.UserDAO
import com.steveperkins.fitnessjiffy.exception.AuthenticationException
import com.steveperkins.fitnessjiffy.domain.User
import com.steveperkins.fitnessjiffy.domain.User.ActivityLevel
import com.steveperkins.fitnessjiffy.domain.User.Gender
import groovy.transform.AutoFinal
import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import org.mindrot.jbcrypt.BCrypt
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service

import javax.servlet.http.HttpServletRequest
import java.security.SecureRandom
import java.sql.SQLException
import java.time.LocalDate
import java.time.ZoneId
import java.util.concurrent.TimeUnit

@Service
@AutoFinal
@CompileStatic
@Slf4j('LOGGER')
class UserService {

    private UserDAO userDAO

    @Value('${secretKey}')
    private String secretKey

    @Autowired
    UserService(UserDAO userDAO) {
        this.userDAO = userDAO
    }

    /**
     * Since the database has a unique constraint on the "email" field, and the UI has an authenticated user's  email
     * readily available via their JWT, this field is used heavily as an auxiliary user ID.
     *
     * @return Empty if no user with this email address was found
     */
    Optional<User> findByEmail(String email) {
        userDAO.findByEmail(email)
    }

    /**
     * <p>New user accounts can be created locally (with password-based authentication), or by way of a 3rd-party
     * identity provider.  In the latter case, the user account will not have a password, because future authentication
     * will be handled by that identity provider.</p>
     *
     * <p>Therefore, user account creation needs to be a two-step process:</p>
     *
     * <ol>
     *     <li>In the first step, the application creates a new account with an email and password (if present),
     *     applying default values for all other fields.</li>
     *     <li>In the second step, the client UI should call "updateUser()" to replace any of those default values.</li>
     * </ol>
     *
     * @param email Supplied from a UI form field when a local account, or a JWT when a 3rd-party identity provider
     * @param password Can be null (for a new account based on 3rd-party authentication)
     * @throws SQLException If the user cannot be created due to a problem with the database (e.g. email already in use)
     */
    void createUser(String email, String password) {
        User user = new User(email: email, passwordHash: hashPassword(password), reportsDirtyDate: LocalDate.now())
        userDAO.create(user)
    }

    /**
     * <p>Updates all user fields, with these exceptions:</p>
     *
     * <ul>
     *     <li><b>"id", "createdTime"</b> - These are effectively immutable.</li>
     *     <li><b>"lastUpdateTime", "reportsDirtyDate"</b> - These are updated automatically with current time values.</li>
     *     <li><b>"passwordHash"</b> - This is meant to be updated via
     *     {@link #changePassword(com.steveperkins.fitnessjiffy.domain.User, java.lang.String, java.lang.String, java.lang.String)}.</li>
     * </ul>
     *
     * <p>If a user changes their email address, then their current authentication session breaks (because it is keyed
     * by email).  The UI should direct the user to logout and re-authenticate when changing their email.</p>
     *
     * <p>If an account was created by way of a 3rd-part identity provider (e.g. Google), then the user's password will
     * be <code>null</code>.  The UI should prompt the user to establish a password <b>before</b> changing their email
     * address, and losing the ability to authenticate with that 3rd-part identity provider.</p>
     *
     * @param user The user to be updated (presumably fetched from {@link #currentAuthenticatedUser} with a JWT)
     * @throws SQLException If the user cannot be updated due to a problem with the database
     */
    void updateUser(
            User user,
            Gender newGender,
            LocalDate newBirthdate,
            Double newHeightInInches,
            ActivityLevel newActivityLevel,
            String newEmail,
            String newFirstName,
            String newLastName,
            ZoneId newTimeZone
    ) throws SQLException {
        user.gender = (newGender != null && newGender != user.gender) ? newGender : user.gender
        user.birthdate = (newBirthdate != null && newBirthdate != user.birthdate) ? newBirthdate : user.birthdate
        user.heightInInches = (newHeightInInches != null && newHeightInInches != user.heightInInches) ? newHeightInInches : user.heightInInches
        user.activityLevel = (newActivityLevel != null && newActivityLevel != user.activityLevel) ? newActivityLevel : user.activityLevel
        user.email = (newEmail != null && newEmail != user.email) ? newEmail : user.email
        user.firstName = (newFirstName != null && newFirstName != user.firstName) ? newFirstName : user.firstName
        user.lastName = (newLastName != null && newLastName != user.lastName) ? newLastName : user.lastName
        user.timeZone = (newTimeZone != null && newTimeZone != user.timeZone) ? newTimeZone : user.timeZone
        user.reportsDirtyDate = LocalDate.now(user.timeZone)

        userDAO.update(user)
    }

    void updateReportsDirtyDate(UUID userId, LocalDate date) {
        Optional<User> user = userDAO.findById(userId)
        if (!user.isPresent()) {
            LOGGER.error('Cannot find user matching id {}', userId)
            return
        }
        LocalDate currentDirtyDate = user.get().reportsDirtyDate
        if (currentDirtyDate == null || currentDirtyDate > date) {
            user.get().reportsDirtyDate = date
            userDAO.update(user.get())
        }
    }

    /**
     * Updates the user's password (or establishes it for the first time, if the user was created through a 3rd-party
     * identity provider with a null password).  It is not necessary for the UI to re-authenticate the user after a
     * password change, but it may choose to do so.
     *
     * @throws SQLException If the password cannot be updated due to a problem with the database
     */
    void changePassword(User user, String currentPassword, String newPassword, String confirmNewPassword) {
        if (newPassword != confirmNewPassword) {
            throw new AuthenticationException('New password and confirmed new password do not match')
        }
        if (!verifyPassword(user.email, currentPassword)) {
            throw new AuthenticationException('The current password is not correct')
        }

        userDAO.changePassword(user, hashPassword(newPassword))
    }

    /**
     * Returns "true" if the email/password combination is valid, or false if the user doesn't exist or can't be authenticated.
     */
    boolean verifyPassword(
            String email,
            String password
    ) {
        Optional<User> user = userDAO.findByEmail(email)
        user.isPresent() && user.get().passwordHash != null
                ? BCrypt.checkpw(password, user.get().passwordHash)
                : null
    }

    /**
     * Retrieves the user record for an HTTP request's authenticated user (i.e. the user whose email address was
     * placed onto the request attributes by {@link com.steveperkins.fitnessjiffy.config.JwtInterceptor}.  Throws
     * an {@link AuthenticationException} if the attribute isn't set, or if there is no matching user in the database.
     */
    User currentAuthenticatedUser(HttpServletRequest request) {
        String email = request.getAttribute('email')
        if ('email' == null) {
            String msg = "No valid 'email' attribute found in request"
            LOGGER.warn(msg)
            throw new AuthenticationException(msg)
        }
        Optional<User> user = findByEmail(email)
        if (!user.isPresent())  {
            String msg = "No local user exists with email: $email"
            LOGGER.warn(msg)
            throw new AuthenticationException(msg)
        }
        user.get()
    }

    /**
     * Constructs a JWT for a user with the given email address, with a 24-hour TTL.
     */
    String buildJWT(String email) {
        // TODO: Add the userId (and timezone, etc?) as a JWT claim
        //       and update "JwtInterceptor" to set it as a request attribute
        //       and eliminate the "currentAuthenticatedUser()" method
        //       and once Google SSO is supported, come up with a way to "wrap" it with a local JWT that includes the userId
        Jwts.builder()
                .setSubject(email as String)
                .setIssuedAt(new Date())
                .setExpiration(new Date(System.currentTimeMillis() + TimeUnit.DAYS.toMillis(1)))
                .signWith(SignatureAlgorithm.HS256, secretKey)
                .compact()
    }

    /**
     * @param rawPassword An unencrypted, plaintext password.
     * @return A one-way hash of the password.
     */
    private static String hashPassword(String rawPassword) {
        if (rawPassword == null) return null
        String salt = BCrypt.gensalt(10, new SecureRandom())
        BCrypt.hashpw(rawPassword, salt)
    }

}
