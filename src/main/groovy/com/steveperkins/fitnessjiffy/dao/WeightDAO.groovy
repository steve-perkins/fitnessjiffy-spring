package com.steveperkins.fitnessjiffy.dao

import com.steveperkins.fitnessjiffy.domain.Weight
import groovy.sql.GroovyRowResult
import groovy.sql.Sql
import groovy.transform.AutoFinal
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

import javax.sql.DataSource
import java.time.LocalDate

import static UuidConverter.toByteArray
import static UuidConverter.toUUID

@Service
@AutoFinal
@CompileStatic
class WeightDAO {

    private DataSource dataSource

    @Autowired
    UserDAO(DataSource dataSource) {
        this.dataSource = dataSource
    }

    void upsert(Weight weight) {
        Sql sql = new Sql(dataSource)
        try {
            String statement = '''
                INSERT INTO weight (id, user_id, date, pounds) VALUES (?, ?, ?, ?) 
                ON DUPLICATE KEY UPDATE pounds = ?
            '''
            sql.execute(statement, [toByteArray(weight.id), toByteArray(weight.userId), weight.date, weight.pounds, weight.pounds] as List<Object>)
        } finally {
            sql.close()
        }
    }

    List<Weight> findAllForUser(UUID userId) {
        Sql sql = new Sql(dataSource)
        try {
            String statement = '''
                SELECT * FROM weight WHERE user_id = ? ORDER BY date DESC
            '''
            sql.rows(statement, [toByteArray(userId)] as List<Object>).collect {
                rowToWeight(it)
            }
        } finally {
            sql.close()
        }
    }

    Weight findByUserAndDate(UUID userId, LocalDate date) {
        Sql sql = new Sql(dataSource)
        try {
            String statement = '''
                SELECT * FROM weight WHERE user_id = ? AND date = ?
            '''
            GroovyRowResult row = sql.firstRow(statement, [toByteArray(userId), date] as List<Object>)
            rowToWeight(row)
        } finally {
            sql.close()
        }
    }

    Weight findByUserMostRecentOnDate(UUID userId, LocalDate date) {
        Sql sql = new Sql(dataSource)
        try {
            String statement = '''
                SELECT weight.* FROM weight, fitnessjiffy_user 
                    WHERE weight.user_id = fitnessjiffy_user.id 
                    AND fitnessjiffy_user.id = ?
                    AND weight.date <= ?
                    ORDER BY weight.date DESC LIMIT 1
            '''
            GroovyRowResult row = sql.firstRow(statement, [toByteArray(userId), date] as List<Object>)
            rowToWeight(row)
        } finally {
            sql.close()
        }
    }

    private static Weight rowToWeight(Map row) {
        row == null
                ? null
                : new Weight(toUUID(row.id as byte[]), toUUID(row.user_id as byte[]), (row.date as java.sql.Date).toLocalDate(), row.pounds as double)
    }

}
