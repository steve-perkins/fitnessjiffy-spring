package com.steveperkins.fitnessjiffy.dao

import com.steveperkins.fitnessjiffy.domain.User
import groovy.sql.Sql
import groovy.transform.AutoFinal
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

import javax.sql.DataSource
import java.sql.Timestamp
import java.time.ZoneId

import static UuidConverter.toByteArray
import static UuidConverter.toUUID

@Service
@AutoFinal
@CompileStatic
class UserDAO {

    private DataSource dataSource

    @Autowired
    UserDAO(DataSource dataSource) {
        this.dataSource = dataSource
    }

    Optional<User> findById(UUID id) {
        Sql sql = new Sql(dataSource)
        try {
            Map row = sql.firstRow("SELECT * FROM fitnessjiffy_user WHERE id = ?", [toByteArray(id)] as List<Object>)
            rowToUser(row)
        } finally {
            sql.close()
        }
    }

    Optional<User> findByEmail(String email) {
        Sql sql = new Sql(dataSource)
        try {
            Map row = sql.firstRow("SELECT * FROM fitnessjiffy_user WHERE email = ?", [email] as List<Object>)
            rowToUser(row)
        } finally {
            sql.close()
        }
    }

    void create(User user) {
        Sql sql = new Sql(dataSource)
        try {
            String statement = '''
                INSERT INTO fitnessjiffy_user 
                    (id, activity_level, birthdate, created_time, email, first_name, gender, height_in_inches, last_name, 
                    last_updated_time, password_hash, timezone, reports_dirty_date) 
                VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
            '''
            sql.execute(statement, [toByteArray(user.id), user.activityLevel.value, user.birthdate, user.createdTime, user.email,
                                    user.firstName, user.gender.toString(), user.heightInInches, user.lastName, user.lastUpdatedTime,
                                    user.passwordHash, user.timeZone.toString(), user.reportsDirtyDate] as List<Object>)
        } finally {
            sql.close()
        }
    }

    void update(User user) {
        Sql sql = new Sql(dataSource)
        try {
            String statement = '''
                UPDATE fitnessjiffy_user 
                SET activity_level = ?, birthdate = ?, email = ?, first_name = ?, gender = ?, height_in_inches = ?,
                    last_name = ?, timezone = ?, reports_dirty_date = ?, last_updated_time = CURRENT_TIMESTAMP
                WHERE id = ?
            '''
            sql.execute(statement, [user.activityLevel.value, user.birthdate, user.email, user.firstName, user.gender.toString(),
                                    user.heightInInches, user.lastName, user.timeZone.toString(), user.reportsDirtyDate,
                                    toByteArray(user.id)] as List<Object>)
        } finally {
            sql.close()
        }
    }

    void changePassword(User user, String newPasswordHash) {
        Sql sql = new Sql(dataSource)
        try {
            String statement = '''
                UPDATE fitnessjiffy_user SET password_hash = ? WHERE id = ?
            '''
            sql.execute(statement, [newPasswordHash, toByteArray(user.id)] as List<Object>)
        } finally {
            sql.close()
        }
    }

    private Optional<User> rowToUser(Map row) {
        row == null ? Optional.empty() : Optional.of(new User(
                toUUID(row.id as byte[]),
                User.Gender.fromString(row.gender as String),
                (row.birthdate as java.sql.Date).toLocalDate(),
                row.height_in_inches as double,
                User.ActivityLevel.fromValue(row.activity_level as double),
                row.email as String,
                row.password_hash as String,
                row.first_name as String,
                row.last_name as String,
                ZoneId.of(row.timezone as String),
                row.created_time as Timestamp,
                row.last_updated_time as Timestamp,
                (row.reports_dirty_date as java.sql.Date).toLocalDate()
        ))
    }

}
