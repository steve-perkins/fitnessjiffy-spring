package com.steveperkins.fitnessjiffy.dao

import com.steveperkins.fitnessjiffy.domain.Exercise
import com.steveperkins.fitnessjiffy.domain.ExercisePerformed
import groovy.sql.Sql
import groovy.transform.AutoFinal
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

import javax.sql.DataSource
import java.time.LocalDate

import static UuidConverter.toByteArray
import static UuidConverter.toUUID

@Service
@AutoFinal
@CompileStatic
class ExerciseDAO {

    private DataSource dataSource

    @Autowired
    UserDAO(DataSource dataSource) {
        this.dataSource = dataSource
    }

    //
    // Exercise
    //

    Optional<Exercise> findById(UUID exerciseId) {
        Sql sql = new Sql(dataSource)
        try {
            String statement = '''
                SELECT * FROM exercise WHERE id = ?
            '''
            Map row = sql.firstRow(statement, [toByteArray(exerciseId)] as List<Object>)
            row == null ? Optional.empty() : Optional.of(rowToExercise(row))
        } finally {
            sql.close()
        }
    }

    List<Exercise> findByDescriptionLike(String partialDescription) {
        if (partialDescription == null || partialDescription.isEmpty()) return []

        Sql sql = new Sql(dataSource)
        try {
            String statement = '''
                SELECT * FROM exercise WHERE LOWER(description) LIKE ? ORDER BY description ASC
            '''
            String param = "%${partialDescription.toLowerCase()}%" // Not sure why this GString doesn't work inline on the line below
            sql.rows(statement, [param] as List<Object>).collect { rowToExercise(it) }
        } finally {
            sql.close()
        }
    }

    List<String> findAllCategories() {
        Sql sql = new Sql(dataSource)
        try {
            String statement = '''
                SELECT DISTINCT(category) FROM exercise ORDER BY category
            '''
            sql.rows(statement).collect { row -> row.category as String }
        } finally {
            sql.close()
        }
    }

    List<Exercise> findByCategory(String category) {
        Sql sql = new Sql(dataSource)
        try {
            String statement = '''
                SELECT * FROM exercise WHERE category = ? ORDER BY DESCRIPTION ASC
            '''
            sql.rows(statement, [category] as List<Object>).collect { rowToExercise(it) }
        } finally {
            sql.close()
        }
    }

    List<Exercise> findByUserPerformedWithinRange(UUID userId, LocalDate startDate, LocalDate endDate) {
        Sql sql = new Sql(dataSource)
        try {
            String statement = '''
                SELECT * FROM exercise WHERE id IN (
                    SELECT DISTINCT exercise_id FROM exercise_performed
                    WHERE user_id = ?
                    AND date BETWEEN ? AND ?
                ) ORDER BY description ASC
            '''
            sql.rows(statement, [toByteArray(userId), startDate, endDate] as List<Object>).collect { rowToExercise(it) }
        } finally {
            sql.close()
        }
    }

    //
    // Exercise Performed
    //

    Optional<ExercisePerformed> findPerformedById(UUID id) {
        Sql sql = new Sql(dataSource)
        try {
            String statement = '''
                SELECT * FROM exercise_performed WHERE ID = ?
            '''
            Map row = sql.firstRow(statement, [toByteArray(id)] as List<Object>)
            row == null ? Optional.empty() : Optional.of(rowToExercisePerformed(row))
        } finally {
            sql.close()
        }

    }

    List<ExercisePerformed> findPerformedByUserAndDate(UUID userId, LocalDate date) {
        Sql sql = new Sql(dataSource)
        try {
            String statement = '''
                SELECT exercise_performed.* FROM exercise_performed, exercise 
                WHERE exercise_performed.exercise_id = exercise.id
                AND exercise_performed.user_id = ? 
                AND exercise_performed.date = ? 
                ORDER BY exercise.description ASC
            '''
            sql.rows(statement, [toByteArray(userId), date] as List<Object>).collect { rowToExercisePerformed(it) }
        } finally {
            sql.close()
        }
    }

    void upsertPerformed(ExercisePerformed exercisePerformed) {
        Sql sql = new Sql(dataSource)
        try {
            String statement = '''
                INSERT INTO exercise_performed (id, user_id, exercise_id, date, minutes) VALUES (?, ?, ?, ?, ?) 
                ON DUPLICATE KEY UPDATE minutes = ?
            '''
            sql.execute(statement, [
                    toByteArray(exercisePerformed.id),
                    toByteArray(exercisePerformed.userId),
                    toByteArray(exercisePerformed.exerciseId),
                    exercisePerformed.date,
                    exercisePerformed.minutes,
                    exercisePerformed.minutes
            ] as List<Object>)
        } finally {
            sql.close()
        }
    }

    void deletePerformed(UUID id) {
        Sql sql = new Sql(dataSource)
        try {
            String statement = '''
                DELETE FROM exercise_performed WHERE id = ?
            '''
            sql.execute(statement, [toByteArray(id)] as List<Object>)
        } finally {
            sql.close()
        }
    }


    private static Exercise rowToExercise(Map row) {
        new Exercise(
                toUUID(row.id as byte[]),
                row.code as String,
                row.metabolic_equivalent as double,
                row.category as String,
                row.description as String
        )
    }

    private static ExercisePerformed rowToExercisePerformed(Map row) {
        new ExercisePerformed(
                toUUID(row.id as byte[]),
                toUUID(row.user_id as byte[]),
                toUUID(row.exercise_id as byte[]),
                (row.date as java.sql.Date).toLocalDate(),
                row.minutes as int
        )
    }

}
