package com.steveperkins.fitnessjiffy.dao

import com.steveperkins.fitnessjiffy.domain.Food
import com.steveperkins.fitnessjiffy.domain.FoodEaten
import groovy.sql.Sql
import groovy.transform.AutoFinal
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

import javax.sql.DataSource
import java.sql.SQLException
import java.sql.Timestamp
import java.time.LocalDate

import static UuidConverter.toByteArray
import static com.steveperkins.fitnessjiffy.dao.UuidConverter.toUUID

@Service
@AutoFinal
@CompileStatic
class FoodDAO {

    private DataSource dataSource

    @Autowired
    FoodDAO(DataSource dataSource) {
        this.dataSource = dataSource
    }

    //
    // Food
    //

    Optional<Food> findFoodById(UUID id) {
        Sql sql = new Sql(dataSource)
        try {
            String statement = '''
                SELECT * FROM food WHERE id = ?
            '''
            Map row = sql.firstRow(statement, [toByteArray(id)] as List<Object>)
            row == null ? Optional.empty() : Optional.of(rowToFood(row))
        } finally {
            sql.close()
        }
    }

    List<Food> searchFoods(UUID userId, String partialName) {
        Sql sql = new Sql(dataSource)
        try {
            String statement = '''
                SELECT * FROM food f 
                WHERE f.owner_id = ? OR (
                    f.owner_id IS NULL 
                    AND NOT EXISTS (SELECT * FROM food subFood WHERE subFood.owner_id = ? AND f.name = subFood.name)
                ) AND LOWER(f.name) LIKE ? 
                ORDER BY f.name ASC
            '''
            String param = "%${partialName.toLowerCase()}%"
            sql.rows(statement, [toByteArray(userId), toByteArray(userId), param] as List<Object>).collect {
                rowToFood(it)
            }
        } finally {
            sql.close()
        }
    }

    List<Food> findByUserPerformedWithinRange(UUID userId, LocalDate startDate, LocalDate endDate) {
        Sql sql = new Sql(dataSource)
        try {
            String statement = '''
                SELECT * FROM food WHERE id IN (
                    SELECT DISTINCT food_id FROM food_eaten
                    WHERE user_id = ?
                    AND date BETWEEN ? AND ?
                ) ORDER BY name ASC 
            '''
            sql.rows(statement, [toByteArray(userId), startDate, endDate] as List<Object>).collect {
                rowToFood(it)
            }
        } finally {
            sql.close()
        }
    }

    void upsertFood(Food food) {
        if (food.ownerId == null) throw new SQLException("Cannot insert or update a global food record (i.e. null owner ID)")
        food.id = food.id ?: UUID.randomUUID()
        Sql sql = new Sql(dataSource)
        try {
            String statement = '''
                INSERT INTO food (
                    id, calories, carbs, created_time, default_serving_type, fat, fiber, last_updated_time, name, 
                    protein, saturated_fat, serving_type_qty, sodium, sugar, owner_id
                ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
                ON DUPLICATE KEY UPDATE calories = ?, carbs = ?, default_serving_type = ?, fat = ?, fiber = ?, 
                    name = ?, protein = ?, saturated_fat = ?, serving_type_qty = ?, sodium = ?, sugar = ?, 
                    last_updated_time = CURRENT_TIMESTAMP
            '''
            sql.execute(statement, [toByteArray(food.id), food.calories, food.carbs, food.createdTime, food.defaultServingType.toString(),
                                    food.fat, food.fiber, food.lastUpdatedTime, food.name, food.protein, food.saturatedFat,
                                    food.servingTypeQty, food.sodium, food.sugar, toByteArray(food.ownerId),
                                    food.calories, food.carbs, food.defaultServingType.toString(), food.fat, food.fiber, food.name,
                                    food.protein, food.saturatedFat, food.servingTypeQty, food.sodium, food.sugar] as List<Object>)
        } finally {
            sql.close()
        }
    }

    //
    // Food Eaten
    //

    Optional<FoodEaten> findEatenById(UUID id) {
        Sql sql = new Sql(dataSource)
        try {
            String statement = '''
                SELECT * FROM food_eaten WHERE id = ?
            '''
            Map row = sql.firstRow(statement, [toByteArray(id)] as List<Object>)
            row == null ? Optional.empty() : Optional.of(rowToFoodEaten(row))
        } finally {
            sql.close()
        }
    }

    List<FoodEaten> findEatenOnDate(UUID userId, LocalDate date) {
        Sql sql = new Sql(dataSource)
        try {
            String statement = '''
                SELECT food_eaten.*, food.name FROM food_eaten, food
                WHERE food_eaten.user_id = ? AND food_eaten.date = ? 
                AND food_eaten.food_id = food.id
                ORDER BY food.name ASC
            '''
            sql.rows(statement, [toByteArray(userId), date] as List<Object>).collect {
                rowToFoodEaten(it)
            }
        } finally {
            sql.close()
        }
    }

    void upsertFoodEaten(FoodEaten foodEaten) {
        foodEaten.id = foodEaten.id ?: UUID.randomUUID()
        Sql sql = new Sql(dataSource)
        try {
            String statement = '''
                INSERT INTO food_eaten (id, food_id, user_id, date, serving_qty, serving_type) 
                VALUES (?, ?, ?, ?, ?, ?)
                ON DUPLICATE KEY UPDATE serving_qty = ?, serving_type = ?
            '''
            sql.execute(statement, [toByteArray(foodEaten.id), toByteArray(foodEaten.foodId), toByteArray(foodEaten.userId),
                                    foodEaten.date, foodEaten.servingQty, foodEaten.servingType.toString(), foodEaten.servingQty,
                                    foodEaten.servingType.toString()] as List<Object>)
        } finally {
            sql.close()
        }
    }

    void deleteFoodEaten(UUID id) {
        Sql sql = new Sql(dataSource)
        try {
            String statement = '''
                DELETE FROM food_eaten WHERE id = ?
            '''
            sql.execute(statement, [toByteArray(id)] as List<Object>)
        } finally {
            sql.close()
        }
    }

    Optional<FoodEaten> findEarliestFoodEaten(UUID userId, UUID foodId) {
        Sql sql = new Sql(dataSource)
        try {
            String statement = '''
                SELECT * FROM food_eaten WHERE user_id = ? AND food_id = ?
                ORDER BY date ASC
                LIMIT 1
            '''
            Map row = sql.firstRow(statement, [toByteArray(userId), toByteArray(foodId)] as List<Object>)
            row == null ? Optional.empty() : Optional.of(rowToFoodEaten(row))
        } finally {
            sql.close()
        }
    }

    private static Food rowToFood(Map row) {
        new Food(
                toUUID(row.id as byte[]),
                toUUID(row.owner_id as byte[]),
                row.name as String,
                Food.ServingType.fromString(row.default_serving_type as String),
                row.serving_type_qty as double,
                row.calories as int,
                row.fat as double,
                row.saturated_fat as double,
                row.carbs as double,
                row.fiber as double,
                row.sugar as double,
                row.protein as double,
                row.sodium as double,
                row.created_time as Timestamp,
                row.last_updated_time as Timestamp
        )
    }

    private static FoodEaten rowToFoodEaten(Map row) {
        new FoodEaten(
                toUUID(row.id as byte[]),
                toUUID(row.user_id as byte[]),
                toUUID(row.food_id as byte[]),
                (row.date as java.sql.Date).toLocalDate(),
                Food.ServingType.fromString(row.serving_type as String),
                row.serving_qty as double,
                ((row.hasProperty('name')) ? row.name : null) as String
        )
    }

}
