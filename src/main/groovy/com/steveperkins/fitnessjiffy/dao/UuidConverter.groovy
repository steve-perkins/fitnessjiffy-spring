package com.steveperkins.fitnessjiffy.dao

import groovy.transform.AutoFinal
import groovy.transform.CompileStatic

import java.nio.ByteBuffer

@AutoFinal
@CompileStatic
class UuidConverter {

    static byte[] toByteArray(UUID uuid) {
        uuid == null ? null : ByteBuffer.wrap(new byte[16]).
                putLong(uuid.getMostSignificantBits()).
                putLong(uuid.getLeastSignificantBits()).
                array()
    }

    static UUID toUUID(byte[] bytes) {
        if (bytes == null) return null
        ByteBuffer buffer = ByteBuffer.wrap(bytes)
        new UUID(buffer.getLong(), buffer.getLong())
    }

}
