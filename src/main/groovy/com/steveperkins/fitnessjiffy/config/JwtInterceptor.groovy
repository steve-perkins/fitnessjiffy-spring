package com.steveperkins.fitnessjiffy.config

import com.steveperkins.fitnessjiffy.exception.AuthenticationException
import com.steveperkins.fitnessjiffy.service.UserService
import groovy.transform.AutoFinal
import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import io.jsonwebtoken.Claims
import io.jsonwebtoken.ExpiredJwtException
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.MalformedJwtException
import io.jsonwebtoken.SignatureException
import io.jsonwebtoken.UnsupportedJwtException
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.web.server.ResponseStatusException
import org.springframework.web.servlet.HandlerInterceptor

import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

/**
 * For incoming HTTP requests that require authentication, this interceptor looks for a valid JWT in the "Authorization"
 * request header.  If found, then the interceptor allows the request to proceed, after adding the user's email address
 * as a request attribute.
 *
 * The JWT provider should be prepended to the encoded JWT string, separated by an underscore.  Initially, only JWT's
 * that were created by this application are supported (i.e. prefix "local_").  Support will be added for JWT's
 * originating from Google (i.e. "google_").
 *
 * {@see WebConfig}
 */
@AutoFinal
@CompileStatic
@Slf4j('LOGGER')
class JwtInterceptor implements HandlerInterceptor {

    private UserService userService

    private String secretKey

    JwtInterceptor(UserService userService, @Value('${secretKey}') String secretKey) {
        this.userService = userService
        this.secretKey = secretKey
    }

    @Override
    boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws AuthenticationException {

        // Bypass authentication checks when creating a new account, or actually performing initial authentication itself.
        //
        // All user CRUD operations use the same URL path, just different HTTP verbs.  Unfortunately, Spring only lets
        // you include or exclude URL paths (not verbs!) when registering interceptors.  So this interceptor must be
        // applied to ALL requests for the user path, and it must take responsibility for skipping the POST ones.
        Closure<Boolean> isAuthentication = { request.requestURI.equalsIgnoreCase('/api/user/auth') && request.method.equalsIgnoreCase('POST') }
        Closure<Boolean> isCreateUser = {
            if (request.requestURI.equalsIgnoreCase('/api/user') && request.method.equalsIgnoreCase('POST')) {
                // TODO: Add concept of "demo mode", where new user account creation is disabled
                true
            } else {
                false
            }
        }
        if (isAuthentication.call() || isCreateUser.call()) {
            return true
        }

        String providerToken = request.getHeader(HttpHeaders.AUTHORIZATION) ?: ''
        String email
        switch (providerToken) {
            case { String t -> t.startsWith("local_") }:
                email = handleLocal(providerToken - 'local_')
                break
            // TODO: support "google_"
            default:
                String msg = 'Authorization header does come from a recognized provider'
                LOGGER.error(msg)
                throw new AuthenticationException(msg)
        }
        if (email == 'demo@demo.com' && ['DELETE', 'POST', 'PUT'].contains(request.method.toUpperCase())) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, 'Demo account has read-only access')
        }
        request.setAttribute('email', email)
        true
    }

    private String handleLocal(String token) {
        try {
            Claims claims = Jwts.parser().setSigningKey(secretKey).parseClaimsJws(token).getBody()
            LOGGER.debug("Authenticated ${claims.sub} with local JWT")
            claims.sub
        } catch (ExpiredJwtException ignored) {
            String msg = 'Expired token'
            LOGGER.error(msg)
            throw new AuthenticationException(msg)
        } catch (UnsupportedJwtException | MalformedJwtException | SignatureException | IllegalArgumentException ignored) {
            final String msg = 'Invalid token'
            LOGGER.error(msg)
            throw new AuthenticationException(msg)
        }
    }

}
