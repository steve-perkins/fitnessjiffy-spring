package com.steveperkins.fitnessjiffy.config

import com.steveperkins.fitnessjiffy.service.UserService
import groovy.transform.CompileStatic
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Configuration
import org.springframework.web.servlet.config.annotation.InterceptorRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer

@Configuration
@CompileStatic
class WebConfig implements WebMvcConfigurer {

    @Autowired
    private UserService userService

    @Value('${secretKey}')
    private String secretKey

    @Override
    void addInterceptors(InterceptorRegistry registry) {
        // Require authentication for all incoming requests.  EXCEPT for initial login requests, because that
        // would create a chicken-and-egg problem.
        registry.addInterceptor(new JwtInterceptor(userService, secretKey))
                .addPathPatterns("/api/**")
                .excludePathPatterns("/api/user/auth")
    }

}
