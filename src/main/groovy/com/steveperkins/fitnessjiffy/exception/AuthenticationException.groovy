package com.steveperkins.fitnessjiffy.exception

import groovy.transform.CompileStatic

/**
 * <p>Custom exception to be thrown in cases where authentication fails.  Meant for use with {@link com.steveperkins.fitnessjiffy.config.ControllerExceptionHandler},
 * to automatically return a 401 status code whenever these exceptions bubble up to the controller layer.</p>
 *
 * <p>One could make an argument that this isn't necessary.  That the code could instead throw a {@link org.springframework.web.server.ResponseStatusException}, with
 * the "status" value set to 401.  See {@link com.steveperkins.fitnessjiffy.controller.ExerciseController} for examples of this.  Eliminating this class and
 * applying that pattern instead would allow {@link com.steveperkins.fitnessjiffy.config.ControllerExceptionHandler} to be eliminated also.</p>
 *
 * <p>However, I'm leaving this in place at least for now, because:</p>
 *
 * <ol>
 *     <li>
 *         A key distinction between this and the {@link com.steveperkins.fitnessjiffy.controller.ExerciseController} examples is that the
 *         latter is only thrown directly from within the controller layer, whereas this custom exception type also bubbles up from the
 *         service layer.  Separation of concerns seems to make it less appropriate for throwers to explicitly set specific HTTP status codes.
 *     </li>
 *     <li>
 *         Why not?  This is at least partly an educational/demo project, so it's worthwhile to have an example of this pattern here also.
 *     </li>
 * </ol>
 */
@CompileStatic
class AuthenticationException extends RuntimeException {

    AuthenticationException(String message) {
        super(message)
    }

}
