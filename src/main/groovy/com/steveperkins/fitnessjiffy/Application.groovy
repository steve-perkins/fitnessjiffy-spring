package com.steveperkins.fitnessjiffy

import groovy.transform.CompileStatic
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
@CompileStatic
class Application {

	// TODO: Add scheduler job, to populate a fresh year's worth of test data for 'demo@demo.com' every 24 hours (?)

	static void main(String[] args) {
		SpringApplication.run(Application, args)
	}

}
