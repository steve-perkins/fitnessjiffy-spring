# noinspection SqlNoDataSourceInspectionForFile

ALTER TABLE `fitnessjiffy`.`fitnessjiffy_user`
ADD UNIQUE INDEX `email_UNIQUE` (`email` ASC);
