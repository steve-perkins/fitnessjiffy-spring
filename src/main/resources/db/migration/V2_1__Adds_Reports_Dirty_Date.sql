ALTER TABLE `fitnessjiffy`.`fitnessjiffy_user` 
ADD COLUMN `reports_dirty_date` DATE NULL AFTER `timezone`;

UPDATE `fitnessjiffy`.`fitnessjiffy_user` SET `reports_dirty_date` = CURRENT_DATE;
