package com.steveperkins.fitnessjiffy

import com.steveperkins.fitnessjiffy.domain.User
import com.wix.mysql.EmbeddedMysql
import com.wix.mysql.config.Charset
import com.wix.mysql.config.MysqldConfig
import groovy.sql.Sql
import groovy.transform.AutoFinal
import groovy.transform.CompileStatic
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.BeforeEach
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.test.context.SpringBootTest

import javax.sql.DataSource
import java.sql.Timestamp
import java.time.ZoneId
import java.util.concurrent.TimeUnit

import static com.steveperkins.fitnessjiffy.dao.UuidConverter.toByteArray
import static com.steveperkins.fitnessjiffy.dao.UuidConverter.toUUID
import static com.wix.mysql.EmbeddedMysql.anEmbeddedMysql
import static com.wix.mysql.ScriptResolver.classPathScript
import static com.wix.mysql.ScriptResolver.classPathScripts
import static com.wix.mysql.config.MysqldConfig.aMysqldConfig
import static com.wix.mysql.distribution.Version.v5_7_latest

/**
 * Provides subclasses with an embedded MySQL database.  In between each JUnit 5 test method, the database gets reset
 * with the same SQL scripts that are normally applied by Flyway.
 */
@SpringBootTest
@AutoFinal
@CompileStatic
abstract class AbstractEmbeddedDatabaseTestSuite {

    static EmbeddedMysql mysql
    static User testUser
    static String testJwt

    @Autowired
    private DataSource dataSource

    @Value('${secretKey}')
    private String secretKey

    @BeforeAll
    static void buildDatabase() {
        MysqldConfig config = aMysqldConfig(v5_7_latest).
                withCharset(Charset.UTF8MB4).
                withPort(13306).
                withUser('fitnessjiffy', 'fitnessjiffy').
                build()
        mysql = anEmbeddedMysql(config).
                addSchema('fitnessjiffy', classPathScripts('db/migration/*.sql')).
                start()
        mysql.executeScripts('fitnessjiffy', classPathScript('test-data.sql'))
    }

    @AfterAll
    static void stopDatabase() {
        mysql.stop()
    }

    @BeforeEach
    void reloadSchema() {
        mysql.reloadSchema("fitnessjiffy", classPathScripts("db/migration/*.sql"))
        mysql.executeScripts('fitnessjiffy', classPathScript('test-data.sql'))
        if (testUser == null || testJwt == null) {
            Sql sql = new Sql(dataSource)
            try {
                Map row = sql.firstRow("SELECT * FROM fitnessjiffy_user WHERE id = ?",
                        [toByteArray(UUID.fromString('2205e7b9-cd44-4417-a45d-96195ca1bd45'))] as List<Object>)
                testUser = new User(
                        toUUID(row.id as byte[]),
                        User.Gender.fromString(row.gender as String),
                        (row.birthdate as java.sql.Date).toLocalDate(),
                        row.height_in_inches as double,
                        User.ActivityLevel.fromValue(row.activity_level as double),
                        row.email as String,
                        row.password_hash as String,
                        row.first_name as String,
                        row.last_name as String,
                        ZoneId.of(row.timezone as String),
                        row.created_time as Timestamp,
                        row.last_updated_time as Timestamp,
                        (row.reports_dirty_date as java.sql.Date).toLocalDate()
                )
                testJwt = Jwts.builder()
                        .setSubject(testUser.email)
                        .setIssuedAt(new Date())
                        .setExpiration(new Date(System.currentTimeMillis() + TimeUnit.DAYS.toMillis(1)))
                        .signWith(SignatureAlgorithm.HS256, secretKey)
                        .compact()
            } finally {
                sql.close()
            }
        }
    }

}