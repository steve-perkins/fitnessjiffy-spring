package com.steveperkins.fitnessjiffy

import com.steveperkins.fitnessjiffy.dao.ExerciseDAO
import com.steveperkins.fitnessjiffy.dao.FoodDAO
import com.steveperkins.fitnessjiffy.dao.UserDAO
import com.steveperkins.fitnessjiffy.dao.WeightDAO
import com.steveperkins.fitnessjiffy.domain.Exercise
import com.steveperkins.fitnessjiffy.domain.ExercisePerformed
import com.steveperkins.fitnessjiffy.domain.Food
import com.steveperkins.fitnessjiffy.domain.FoodEaten
import com.steveperkins.fitnessjiffy.domain.User
import com.steveperkins.fitnessjiffy.domain.Weight
import groovy.transform.AutoFinal
import groovy.transform.CompileStatic
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired

import java.sql.SQLException
import java.time.LocalDate

import static org.junit.jupiter.api.Assertions.assertEquals
import static org.junit.jupiter.api.Assertions.assertFalse
import static org.junit.jupiter.api.Assertions.assertNotEquals
import static org.junit.jupiter.api.Assertions.assertNotNull
import static org.junit.jupiter.api.Assertions.assertNull
import static org.junit.jupiter.api.Assertions.assertThrows
import static org.junit.jupiter.api.Assertions.assertTrue

/**
 * A collection of integration tests that exercises all DAO methods, with a complete Spring application context and an
 * embedded MySQL database.
 *
 * Ideally, this would be broken up into multiple classes.  However, the time cost of launching the embedded MySQL
 * database is quite high, so all of the tests are co-located in order to pay that cost just one time rather than several.
 * The tests are still grouped by JUnit 5 nested classes for an acceptable level of organization.
 */
@AutoFinal
@CompileStatic
class DAOIntegrationTests extends AbstractEmbeddedDatabaseTestSuite {

    @Autowired
    ExerciseDAO exerciseDAO

    @Autowired
    FoodDAO foodDAO

    @Autowired
    UserDAO userDAO

    @Autowired
    WeightDAO weightDAO

    @Nested
    @DisplayName('ExerciseDAO')
    class ExerciseDAOTests {

        //
        // Exercise
        //

        @Test
        void 'Find all exercise categories'() {
            List<String> categories = exerciseDAO.findAllCategories()

            assertEquals(21, categories.size())
            assertEquals('bicycling', categories.first())
            assertEquals('winter activities', categories.last())
        }

        @Test
        void 'Find exercises by category'() {
            List<Exercise> shouldBeEmpty = exerciseDAO.findByCategory(null)
            List<Exercise> shouldBePopulated = exerciseDAO.findByCategory('bicycling')

            assertNotNull shouldBeEmpty
            assertTrue shouldBeEmpty.isEmpty()

            assertEquals(18, shouldBePopulated.size())
            assertEquals('bicycling, 10-11.9 mph, leisure, slow, light effort', shouldBePopulated.first().description)
            assertEquals('unicycling', shouldBePopulated.last().description)
        }

        @Test
        void 'Find exercises by description'() {
            List<Exercise> shouldBeEmpty = exerciseDAO.findByDescriptionLike(null)
            List<Exercise> shouldBePopulated = exerciseDAO.findByDescriptionLike('swim')
            List<Exercise> caseInsensitive = exerciseDAO.findByDescriptionLike('SwIm')

            assertNotNull shouldBeEmpty
            assertTrue shouldBeEmpty.isEmpty()

            assertEquals(16, shouldBePopulated.size())
            assertEquals('coaching, football, soccer, basketball, baseball, swimming, etc.', shouldBePopulated.first().description)
            assertEquals('swimming, treading water, moderate effort, general', shouldBePopulated.last().description)

            assertEquals(shouldBePopulated, caseInsensitive)
        }

        @Test
        void 'Find exercises within a date range'() {
            // Create exercise_performed records... one month back, one week back, and on current day.
            Exercise bicycling = exerciseDAO.findByDescriptionLike('bicycling, 10-11.9 mph, leisure, slow, light effort').first()
            Exercise swimming = exerciseDAO.findByDescriptionLike('swimming, treading water, moderate effort, general').first()
            Exercise unicycling = exerciseDAO.findByDescriptionLike('unicycling').first()
            ExercisePerformed bicyclingPerformed = new ExercisePerformed(userId: testUser.id, exerciseId: bicycling.id, date: LocalDate.now() - 30, minutes: 30)
            ExercisePerformed swimmingPerformed = new ExercisePerformed(userId: testUser.id, exerciseId: swimming.id, date: LocalDate.now() - 7, minutes: 30)
            ExercisePerformed unicyclingPerformed = new ExercisePerformed(userId: testUser.id, exerciseId: unicycling.id, date: LocalDate.now(), minutes: 30)
            exerciseDAO.upsertPerformed(bicyclingPerformed)
            exerciseDAO.upsertPerformed(swimmingPerformed)
            exerciseDAO.upsertPerformed(unicyclingPerformed)

            // Find exercises performed within the past two weeks
            List<Exercise> exercises = exerciseDAO.findByUserPerformedWithinRange(testUser.id, LocalDate.now() - 14, LocalDate.now())

            // Verify that this includes only the ones from today and last week
            assertEquals(2, exercises.size())
            assertEquals(swimming, exercises.first())
            assertEquals(unicycling, exercises.last())
        }

        //
        // Exercise Performed
        //

        @Test
        void 'Create and delete new exercise_performed record'() {
            LocalDate testDate = LocalDate.now(testUser.timeZone)
            Exercise testExercise = exerciseDAO.findByDescriptionLike('unicycling').first()
            ExercisePerformed exercisePerformed = new ExercisePerformed(userId: testUser.id, exerciseId: testExercise.id, date: testDate, minutes: 30)

            exerciseDAO.upsertPerformed(exercisePerformed)

            List<ExercisePerformed> retrieved = exerciseDAO.findPerformedByUserAndDate(testUser.id, testDate)
            assertEquals(1, retrieved.size())
            assertEquals(exercisePerformed, retrieved.first())

            exerciseDAO.deletePerformed(retrieved.first().id)

            assertEquals([], exerciseDAO.findPerformedByUserAndDate(testUser.id, testDate))
        }

        @Test
        void 'Update exercise_performed record'() {
            Exercise testExercise = exerciseDAO.findByDescriptionLike('unicycling').first()
            ExercisePerformed exercisePerformed = new ExercisePerformed(userId: testUser.id, exerciseId: testExercise.id, date: LocalDate.now(testUser.timeZone), minutes: 30)
            exerciseDAO.upsertPerformed(exercisePerformed)

            exercisePerformed.minutes = 60
            exerciseDAO.upsertPerformed(exercisePerformed)

            Optional<ExercisePerformed> retrieved = exerciseDAO.findPerformedById(exercisePerformed.id)
            assertTrue retrieved.isPresent()
            assertEquals(exercisePerformed, retrieved.get())
            assertEquals(60, retrieved.get().minutes)
        }

    }

    @Nested
    @DisplayName('FoodDAO')
    class FoodDAOTests {

        @Nested
        @DisplayName('Food')
        class FoodTests {

            @Nested
            @DisplayName('Upsert')
            class UpsertTests {

                @Test
                void 'Create completely new food'() {
                    Food userCustomFood = new Food(UUID.randomUUID(), testUser.id, 'Test Food', Food.ServingType.OUNCE,
                            1, 100, 1, 1, 1, 1, 1, 1, 1)

                    foodDAO.upsertFood(userCustomFood)

                    Food retrieved = foodDAO.searchFoods(testUser.id, 'Test Food').first()
                    assertEquals(100, retrieved.calories)
                }

                @Test
                void 'Create custom copy of global food'() {
                    Food globalFood = foodDAO.searchFoods(testUser.id, 'Captain D Battered Fish Fillet').first()
                    assertNull(globalFood.ownerId)
                    assertEquals('Captain D Battered Fish Fillet', globalFood.name)
                    assertNotEquals(100, globalFood.calories)

                    globalFood.id = UUID.randomUUID()
                    globalFood.ownerId = testUser.id
                    globalFood.calories = 100
                    foodDAO.upsertFood(globalFood)
                    List<Food> userCustomFoods = foodDAO.searchFoods(testUser.id, 'Captain D Battered Fish Fillet')

                    assertEquals(1, userCustomFoods.size())  // Results do NOT include the original global food record
                    assertEquals('Captain D Battered Fish Fillet', userCustomFoods.first().name)
                    assertEquals(testUser.id, userCustomFoods.first().ownerId)
                    assertEquals(100, userCustomFoods.first().calories)
                }

                @Test
                void 'Cannot update global food while owner_id is still null'() {
                    assertThrows(SQLException.class) {
                        Food globalFood = foodDAO.searchFoods(testUser.id, 'Captain D Battered Fish Fillet').first()

                        globalFood.calories = 100
                        foodDAO.upsertFood(globalFood)
                    }
                }

                @Test
                void 'Update existing custom food'() {
                    Food globalFood = foodDAO.searchFoods(testUser.id, 'Captain D Battered Fish Fillet').first()
                    globalFood.id = UUID.randomUUID()
                    globalFood.ownerId = testUser.id
                    globalFood.calories = 100
                    foodDAO.upsertFood(globalFood)
                    Food userCustomFood = foodDAO.searchFoods(testUser.id, 'Captain D Battered Fish Fillet').first()

                    userCustomFood.name = 'Updated food name'
                    foodDAO.upsertFood(userCustomFood)
                    List<Food> retrieved = foodDAO.searchFoods(testUser.id, 'Updated food name')

                    assertEquals(1, retrieved.size())
                    assertEquals('Updated food name', retrieved.first().name)
                    assertEquals(testUser.id, retrieved.first().ownerId)
                    assertEquals(100, retrieved.first().calories)
                }

            }

            @Nested
            @DisplayName('Search')
            class SearchTests {

                @Test
                void 'Find food by ID'() {
                    Food userCustomFood = new Food(UUID.randomUUID(), testUser.id, 'Test Food', Food.ServingType.OUNCE,
                            1, 100, 1, 1, 1, 1, 1, 1, 1)
                    foodDAO.upsertFood(userCustomFood)

                    Optional<Food> noSuchFood = foodDAO.findFoodById(UUID.randomUUID())
                    Optional<Food> retrieved = foodDAO.findFoodById(userCustomFood.id)

                    assertFalse noSuchFood.isPresent()
                    // Unfortunately, can't just "equals()" the objects wholesale, because MySQL strips the nanoseconds from the Timestamp fields
                    assertEquals(userCustomFood.id, retrieved.get().id)
                    assertEquals(userCustomFood.name, retrieved.get().name)
                }

                @Test
                void "Search results do not include other user's foods"() {
                    List<Food> globalFoods = foodDAO.searchFoods(testUser.id, 'fish')
                    assertEquals(4, globalFoods.size())
                    assertEquals('Captain D Battered Fish Fillet', globalFoods.first().name)
                    assertEquals('Parmesan Encrusted Tilapia fish fillets', globalFoods.last().name)

                    User otherUser = new User(email: 'other@other.com')
                    userDAO.create(otherUser)
                    Food otherUserFood = globalFoods.first()
                    otherUserFood.id = UUID.randomUUID()
                    otherUserFood.ownerId = otherUser.id
                    otherUserFood.calories = 100
                    foodDAO.upsertFood(otherUserFood)

                    globalFoods = foodDAO.searchFoods(testUser.id, 'fish')
                    assertEquals(4, globalFoods.size())
                    assertEquals('Captain D Battered Fish Fillet', globalFoods.first().name)
                    assertEquals('Parmesan Encrusted Tilapia fish fillets', globalFoods.last().name)
                    assertNotEquals(100, globalFoods.first().calories)
                }

                @Test
                void 'Find foods eaten within a date range'() {
                    List<Food> globalFoods = foodDAO.searchFoods(testUser.id, 'fish')
                    FoodEaten eatenLastWeek = new FoodEaten(UUID.randomUUID(), testUser.id, globalFoods[0].id,
                            LocalDate.now(testUser.timeZone) - 6, globalFoods[0].defaultServingType, globalFoods[0].servingTypeQty)
                    FoodEaten eatenLastMonth = new FoodEaten(UUID.randomUUID(), testUser.id, globalFoods[1].id,
                            LocalDate.now(testUser.timeZone) - 29, globalFoods[0].defaultServingType, globalFoods[1].servingTypeQty)
                    foodDAO.upsertFoodEaten(eatenLastWeek)
                    foodDAO.upsertFoodEaten(eatenLastMonth)

                    List<Food> eatenLastTwoWeeks = foodDAO.findByUserPerformedWithinRange(testUser.id, LocalDate.now(testUser.timeZone) - 13, LocalDate.now(testUser.timeZone))

                    assertEquals(1, eatenLastTwoWeeks.size())
                    assertEquals(eatenLastWeek.foodId, eatenLastTwoWeeks[0].id)
                }

            }

        }

        @Nested
        @DisplayName('Food Eaten')
        class FoodEatenTests {

            @Test
            void 'Find eaten on date'() {
                Food testFood = foodDAO.searchFoods(testUser.id, 'fish').first()
                FoodEaten foodEaten = new FoodEaten(UUID.randomUUID(), testUser.id, testFood.id, LocalDate.now(testUser.timeZone),
                        testFood.defaultServingType, testFood.servingTypeQty)
                foodDAO.upsertFoodEaten(foodEaten)

                List<FoodEaten> retrieved = foodDAO.findEatenOnDate(testUser.id, LocalDate.now(testUser.timeZone))

                assertEquals(1, retrieved.size())
                assertEquals(testFood.id, retrieved.first().foodId)
            }

            @Test
            void 'Create, update, and delete'() {
                Food testFood = new Food(UUID.randomUUID(), testUser.id, 'Test Food', Food.ServingType.OUNCE, 1, 1, 1, 1, 1, 1, 1, 1, 1)
                foodDAO.upsertFood(testFood)
                FoodEaten foodEaten = new FoodEaten(UUID.randomUUID(), testUser.id, testFood.id, LocalDate.now(testUser.timeZone), testFood.defaultServingType, testFood.servingTypeQty)
                foodDAO.upsertFoodEaten(foodEaten)

                Optional<FoodEaten> retrieved = foodDAO.findEatenById(foodEaten.id)
                assertTrue retrieved.isPresent()
                retrieved.get().servingQty = 100
                foodDAO.upsertFoodEaten(retrieved.get())

                List<FoodEaten> updated = foodDAO.findEatenOnDate(testUser.id, LocalDate.now(testUser.timeZone))
                assertEquals(1, updated.size())
                assertEquals(testFood.id, updated.first().foodId)
                assertEquals(100, updated.first().servingQty)

                foodDAO.deleteFoodEaten(updated.first().id)

                assertEquals([], foodDAO.findEatenOnDate(testUser.id, LocalDate.now(testUser.timeZone)))
            }

            @Test
            void 'Find earliest food eaten record for food'() {
                Food testFood = new Food(UUID.randomUUID(), testUser.id, 'Test Food', Food.ServingType.OUNCE, 1, 1, 1, 1, 1, 1, 1, 1, 1)
                foodDAO.upsertFood(testFood)
                FoodEaten eatenLastWeek = new FoodEaten(UUID.randomUUID(), testUser.id, testFood.id, LocalDate.now(testUser.timeZone) - 6, testFood.defaultServingType, 1)
                FoodEaten eatenLastMonth = new FoodEaten(UUID.randomUUID(), testUser.id, testFood.id, LocalDate.now(testUser.timeZone) - 29, testFood.defaultServingType, 2)
                foodDAO.upsertFoodEaten(eatenLastWeek)
                foodDAO.upsertFoodEaten(eatenLastMonth)

                Optional<FoodEaten> earliestEaten = foodDAO.findEarliestFoodEaten(testUser.id, testFood.id)

                assertTrue earliestEaten.isPresent()
                assertEquals(eatenLastMonth.date, earliestEaten.get().date)
                assertEquals(eatenLastMonth.servingQty, earliestEaten.get().servingQty)
            }

        }

    }

    @Nested
    @DisplayName('UserDAO')
    class UserDAOTests {

        @Test
        void 'Find user by email'() {
            Optional<User> userByEmail = userDAO.findByEmail('demo@demo.com')
            Optional<User> shouldBeNull = userDAO.findByEmail(null)
            Optional<User> noSuchUser = userDAO.findByEmail('unknown@unknown.com')

            assertTrue userByEmail.isPresent()
            assertFalse shouldBeNull.isPresent()
            assertFalse noSuchUser.isPresent()
        }

        @Test
        void 'Create new user'() {
            User newUser = new User(email: 'new@new.com', passwordHash: 'this-is-a-fake-password-hash')
            userDAO.create(newUser)
            Optional<User> retrievedUser = userDAO.findByEmail('new@new.com')
            // Not EXACTLY expected.  Email and passwordHash have no default value, while the generated ID and
            // timestamp fields will be slightly different.
            User expectedUser = new User()

            assertTrue retrievedUser.isPresent()
            assertEquals(expectedUser.gender, retrievedUser.get().gender)
            assertEquals(expectedUser.birthdate.toString(), retrievedUser.get().birthdate.toString())
            assertEquals(expectedUser.heightInInches, retrievedUser.get().heightInInches)
            assertEquals(expectedUser.activityLevel, retrievedUser.get().activityLevel)
            assertEquals(expectedUser.firstName, retrievedUser.get().firstName)
            assertEquals(expectedUser.lastName, retrievedUser.get().lastName)
            assertEquals(expectedUser.timeZone, retrievedUser.get().timeZone)
            assertEquals(expectedUser.reportsDirtyDate.toString(), retrievedUser.get().reportsDirtyDate.toString())
        }

        @Test
        void 'Update user'() {
            User newUser = new User(email: 'new@new.com', passwordHash: 'this-is-a-fake-password-hash')
            userDAO.create(newUser)
            User user = userDAO.findByEmail('new@new.com').get()
            user.email = 'updated@updated.com'
            user.activityLevel = User.ActivityLevel.EXTREMELY_ACTIVE

            sleep(2000)
            userDAO.update(user)
            Optional<User> retrievedUser = userDAO.findById(user.id)

            assertTrue retrievedUser.isPresent()
            // These fields changed
            assertEquals('updated@updated.com', retrievedUser.get().email)
            assertEquals(User.ActivityLevel.EXTREMELY_ACTIVE, retrievedUser.get().activityLevel)
            assertNotEquals(user.lastUpdatedTime, retrievedUser.get().lastUpdatedTime)
            // These fields didn't
            assertEquals(user.gender, retrievedUser.get().gender)
            assertEquals(user.birthdate.toString(), retrievedUser.get().birthdate.toString())
            assertEquals(user.heightInInches, retrievedUser.get().heightInInches)
            assertEquals(user.firstName, retrievedUser.get().firstName)
            assertEquals(user.lastName, retrievedUser.get().lastName)
            assertEquals(user.timeZone, retrievedUser.get().timeZone)
        }

        @Test
        void 'Change password'() {
            userDAO.changePassword(testUser, 'new-password-hash')
            User retrievedUser = userDAO.findByEmail('test@test.com').get()

            assertEquals('new-password-hash', retrievedUser.passwordHash)
        }

    }

    @Nested
    @DisplayName('WeightDAO')
    class WeightDAOTests {

        @Test
        void 'Create new weight record'() {
            Weight weight = new Weight(UUID.randomUUID(), testUser.id, LocalDate.now(), 150)

            weightDAO.upsert(weight)

            Weight retrievedWeight = weightDAO.findByUserAndDate(testUser.id, LocalDate.now())
            assertEquals(weight, retrievedWeight)
        }

        @Test
        void 'Update weight record, with an ID match'() {
            Weight weight = new Weight(UUID.randomUUID(), testUser.id, LocalDate.now(), 150)
            weightDAO.upsert(weight)

            weight.pounds = 160
            weightDAO.upsert(weight)

            Weight retrievedWeight = weightDAO.findByUserAndDate(testUser.id, LocalDate.now())
            assertEquals(weight, retrievedWeight)
        }

        @Test
        void 'Update weight record, without an ID match'() {
            // This is typically how updates will work, since the UI client isn't provided with a "Weight" object's "id" field.
            // The DAO layer has to recognize when a record already exists for a given userId and date, and re-use its ID.
            Weight weight = new Weight(UUID.randomUUID(), testUser.id, LocalDate.now(), 150)
            weightDAO.upsert(weight)

            Weight updatedWeight = new Weight(UUID.randomUUID(), testUser.id, LocalDate.now(), 160)
            weightDAO.upsert(updatedWeight)

            Weight retrievedWeight = weightDAO.findByUserAndDate(testUser.id, LocalDate.now())
            assertEquals(weight.id, retrievedWeight.id)
            assertEquals(weight.userId, retrievedWeight.userId)
            assertEquals(weight.date, retrievedWeight.date)
            assertEquals(updatedWeight.pounds, retrievedWeight.pounds)
        }

        @Test
        void 'Find all for user'() {
            LocalDate today = LocalDate.now()
            LocalDate lastMonth = today - 30
            (lastMonth..today).each { date ->
                Weight weight = new Weight(UUID.randomUUID(), testUser.id, date, 150)
                weightDAO.upsert(weight)
            }

            List<Weight> weights = weightDAO.findAllForUser(testUser.id)

            assertEquals(31, weights.size())
            assertEquals(today, weights.first().date)
            assertEquals(lastMonth, weights.last().date)
        }

        @Test
        void 'Find most recent for user on date'() {
            // Create weight records starting one month ago, but stopping one week ago
            LocalDate today = LocalDate.now()
            LocalDate lastWeek = today - 7
            LocalDate lastMonth = today - 30
            (lastMonth..lastWeek).each { date ->
                Weight weight = new Weight(UUID.randomUUID(), testUser.id, date, 150)
                weightDAO.upsert(weight)
            }

            // Request a weight record for yesterday's date (no such record actually exists)
            LocalDate yesterday = today - 1
            Weight yesterdaysWeight = weightDAO.findByUserMostRecentOnDate(testUser.id, yesterday)

            // Confirm that there IS a record (i.e. the most recent entry from a week ago)
            assertNotNull yesterdaysWeight
            assertEquals(lastWeek, yesterdaysWeight.date)
        }

    }

}
