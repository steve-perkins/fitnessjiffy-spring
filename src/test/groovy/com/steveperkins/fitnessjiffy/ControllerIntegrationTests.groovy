package com.steveperkins.fitnessjiffy

import com.fasterxml.jackson.databind.ObjectMapper
import com.steveperkins.fitnessjiffy.domain.Exercise
import com.steveperkins.fitnessjiffy.domain.ExercisePerformed
import com.steveperkins.fitnessjiffy.domain.Food
import com.steveperkins.fitnessjiffy.domain.FoodEaten
import com.steveperkins.fitnessjiffy.domain.Weight
import com.steveperkins.fitnessjiffy.service.ExerciseService
import com.steveperkins.fitnessjiffy.service.FoodService
import com.steveperkins.fitnessjiffy.service.UserService
import com.steveperkins.fitnessjiffy.service.WeightService
import groovy.json.JsonSlurper
import groovy.transform.AutoFinal
import groovy.transform.CompileDynamic
import groovy.transform.CompileStatic
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder
import org.springframework.web.util.NestedServletException

import java.nio.charset.StandardCharsets
import java.sql.SQLIntegrityConstraintViolationException
import java.time.LocalDate

import static org.hamcrest.Matchers.containsString
import static org.junit.jupiter.api.Assertions.assertEquals
import static org.junit.jupiter.api.Assertions.assertNotEquals
import static org.junit.jupiter.api.Assertions.assertTrue
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

/**
 * A collection of integration tests that exercises all controller endpoints, with a complete Spring application context
 * and an embedded MySQL database.
 *
 * Ideally, this would be broken up into multiple classes.  However, the time cost of launching the embedded MySQL
 * database is quite high, so all of the tests are co-located in order to pay that cost just one time rather than several.
 * The tests are still grouped by JUnit 5 nested classes for an acceptable level of organization.
 */
@AutoConfigureMockMvc
@AutoFinal
@CompileStatic
class ControllerIntegrationTests extends AbstractEmbeddedDatabaseTestSuite {

    @Autowired
    private MockMvc mockMvc

    @Autowired
    private ExerciseService exerciseService

    @Autowired
    private FoodService foodService

    @Autowired
    private UserService userService

    @Autowired
    private WeightService weightService

    private String getJwt(String email, String password) {
        MockHttpServletRequestBuilder request = post('/api/user/auth').
                contentType(MediaType.APPLICATION_JSON).
                content(/{"email":"$email", "password":"$password"}/)
        String payload = mockMvc.perform(request).andReturn().response.contentAsString
        Map json = new JsonSlurper().parseText(payload) as Map
        json.token
    }

    @Nested
    @DisplayName('Exercise Controller')
    class ExerciseControllerTests {

        @Nested
        @DisplayName('Exercise')
        class ExerciseTests {

            @Test
            void 'Returns 200 with exercises matching description'() {
                MockHttpServletRequestBuilder request = get('/api/exercise/swimming').
                        header(HttpHeaders.AUTHORIZATION, "local_$testJwt")
                String payload = mockMvc.perform(request).
                        andExpect(status().isOk()).
                        andReturn().response.contentAsString
                List<Map> exercises = new JsonSlurper().parseText(payload) as List<Map>
                assertEquals(16, exercises.size())
            }

            @Test
            void 'Returns 404 when no exercises match description'() {
                String url = "/api/exercise/${URLEncoder.encode('no such description', StandardCharsets.UTF_8.toString())}"
                MockHttpServletRequestBuilder request = get(url).
                        header(HttpHeaders.AUTHORIZATION, "local_$testJwt")
                mockMvc.perform(request).
                        andExpect(status().is(HttpStatus.NOT_FOUND.value()))
            }

            @Test
            void 'Returns 200 with exercise categories'() {
                MockHttpServletRequestBuilder request = get('/api/exercise/category').
                        header(HttpHeaders.AUTHORIZATION, "local_$testJwt")
                String payload = mockMvc.perform(request).
                        andExpect(status().isOk()).
                        andReturn().response.contentAsString
                List<String> exercises = new JsonSlurper().parseText(payload) as List<String>
                assertEquals(21, exercises.size())
            }

            @Test
            void 'Returns 200 with exercises in category'() {
                MockHttpServletRequestBuilder request = get('/api/exercise/category/running').
                        header(HttpHeaders.AUTHORIZATION, "local_$testJwt")
                String payload = mockMvc.perform(request).
                        andExpect(status().isOk()).
                        andReturn().response.contentAsString
                List<Map> exercises = new JsonSlurper().parseText(payload) as List<Map>

                assertEquals(25, exercises.size())
            }

            @Test
            void 'Returns 404 when no exercises in category'() {
                MockHttpServletRequestBuilder request = get('/api/exercise/category/unknown').
                        header(HttpHeaders.AUTHORIZATION, "local_$testJwt")
                mockMvc.perform(request).
                        andExpect(status().is(HttpStatus.NOT_FOUND.value())).
                        andExpect(content().string(''))
            }

            @Test
            void 'Returns 200 with recent exercises'() {
                Exercise unicycling = exerciseService.findByDescriptionLike('unicycling').first()
                ExercisePerformed exerciseLastWeek = new ExercisePerformed(userId: testUser.id, exerciseId: unicycling.id, date: LocalDate.now() - 7, minutes: 30)
                exerciseService.upsertPerformed(exerciseLastWeek)

                MockHttpServletRequestBuilder request = get("/api/exercise/recent/${LocalDate.now() - 14}").
                        header(HttpHeaders.AUTHORIZATION, "local_$testJwt")
                String payload = mockMvc.perform(request).
                        andExpect(status().isOk()).
                        andReturn().response.contentAsString
                List<Map> exercises = new JsonSlurper().parseText(payload) as List<Map>

                assertEquals(1, exercises.size())
                assertEquals(unicycling.id, UUID.fromString(exercises.first().id as String))
            }

            @Test
            void 'Returns 404 when no recent exercises'() {
                MockHttpServletRequestBuilder request = get("/api/exercise/recent/${LocalDate.now() - 14}").
                        header(HttpHeaders.AUTHORIZATION, "local_$testJwt")
                mockMvc.perform(request).
                        andExpect(status().is(HttpStatus.NOT_FOUND.value()))
            }

        }

        @Nested
        @DisplayName('Exercise Performed')
        class ExercisePerformedTests {

            @Test
            void 'Returns 200 and exercises performed, when date is specified'() {
                Weight weight = new Weight(UUID.randomUUID(), testUser.id, LocalDate.now(testUser.timeZone) - 1, 150)
                weightService.upsert(weight)

                Exercise unicycling = exerciseService.findByDescriptionLike('unicycling').first()
                ExercisePerformed exercisePerformed = new ExercisePerformed(userId: testUser.id, exerciseId: unicycling.id, date: LocalDate.now() - 1, minutes: 30)
                exerciseService.upsertPerformed(exercisePerformed)

                MockHttpServletRequestBuilder request = get("/api/exercise/performed/${LocalDate.now() - 1}").
                        header(HttpHeaders.AUTHORIZATION, "local_$testJwt")
                String payload = mockMvc.perform(request).
                        andExpect(status().isOk()).
                        andReturn().response.contentAsString
                List<Map> exercisesPerformed = new JsonSlurper().parseText(payload) as List<Map>
                assertEquals(1, exercisesPerformed.size())
            }

            @Test
            void "Returns 200 and today's exercises performed, when no date specified"() {
                Weight weight = new Weight(UUID.randomUUID(), testUser.id, LocalDate.now(testUser.timeZone), 150)
                weightService.upsert(weight)

                Exercise unicycling = exerciseService.findByDescriptionLike('unicycling').first()
                ExercisePerformed exercisePerformed = new ExercisePerformed(userId: testUser.id, exerciseId: unicycling.id, date: LocalDate.now(), minutes: 30)
                exerciseService.upsertPerformed(exercisePerformed)

                MockHttpServletRequestBuilder request = get("/api/exercise/performed").
                        header(HttpHeaders.AUTHORIZATION, "local_$testJwt")
                String payload = mockMvc.perform(request).
                        andExpect(status().isOk()).
                        andReturn().response.contentAsString
                List<Map> exercisesPerformed = new JsonSlurper().parseText(payload) as List<Map>
                assertEquals(1, exercisesPerformed.size())
            }

            @Test
            void 'Returns 400 when date cannot be parsed'() {
                String url = "/api/exercise/performed/${URLEncoder.encode('this will not parse as a date!', StandardCharsets.UTF_8.toString())}"
                MockHttpServletRequestBuilder request = get(url).
                        header(HttpHeaders.AUTHORIZATION, "local_$testJwt")
                mockMvc.perform(request).
                        andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
            }

            @Test
            void 'Returns 404 when no exercises performed on date'() {
                MockHttpServletRequestBuilder request = get("/api/exercise/performed").
                        header(HttpHeaders.AUTHORIZATION, "local_$testJwt")
                mockMvc.perform(request).
                        andExpect(status().is(HttpStatus.NOT_FOUND.value()))
            }

            @Test
            void 'Returns 200 on successful create'() {
                Weight weight = new Weight(UUID.randomUUID(), testUser.id, LocalDate.now(testUser.timeZone), 150)
                weightService.upsert(weight)
                Exercise testExercise = exerciseService.findByDescriptionLike('unicycling').first()

                MockHttpServletRequestBuilder request = post('/api/exercise/performed').
                        contentType(MediaType.APPLICATION_JSON).
                        content(/{"exerciseId":"${testExercise.id}", "date":"${LocalDate.now()}", "minutes":30}/).
                        header(HttpHeaders.AUTHORIZATION, "local_$testJwt")
                String content = mockMvc.perform(request).
                        andExpect(status().isOk()).
                        andReturn().response.contentAsString
                Map payload = new JsonSlurper().parseText(content) as Map

                List<ExercisePerformed> retrieved = exerciseService.findByUserOnDate(testUser.id, LocalDate.now())
                assertEquals(1, retrieved.size())
                assertEquals(testExercise.id, retrieved.first().exerciseId)
                assertEquals(retrieved.first().id.toString(), payload.id)
            }

            @Test
            void 'Return 403 on attempted create by demo user'() {
                String jwt = getJwt('demo@demo.com', 'password')
                MockHttpServletRequestBuilder request = post('/api/exercise/performed').
                        contentType(MediaType.APPLICATION_JSON).
                        content(/{"exerciseId":"${UUID.randomUUID()}", "date":"${LocalDate.now()}", "minutes":30}/).
                        header(HttpHeaders.AUTHORIZATION, "local_$jwt")
                mockMvc.perform(request).
                        andExpect(status().is(HttpStatus.FORBIDDEN.value()))
            }

            @Test
            void 'Returns 200 on successful update'() {
                Weight weight = new Weight(UUID.randomUUID(), testUser.id, LocalDate.now(testUser.timeZone), 150)
                weightService.upsert(weight)

                Exercise testExercise = exerciseService.findByDescriptionLike('unicycling').first()
                ExercisePerformed exercisePerformed = new ExercisePerformed(userId: testUser.id, exerciseId: testExercise.id, date: LocalDate.now(testUser.timeZone), minutes: 30)
                exerciseService.upsertPerformed(exercisePerformed)

                MockHttpServletRequestBuilder request = put('/api/exercise/performed').
                        contentType(MediaType.APPLICATION_JSON).
                        content(/{"exerciseId":"${testExercise.id}", "date":"${LocalDate.now(testUser.timeZone)}", "minutes":60}/).
                        header(HttpHeaders.AUTHORIZATION, "local_$testJwt")
                mockMvc.perform(request).
                        andExpect(status().isOk())

                List<ExercisePerformed> retrieved = exerciseService.findByUserOnDate(testUser.id, LocalDate.now())
                assertEquals(1, retrieved.size())
                assertEquals(testExercise.id, retrieved.first().exerciseId)
                assertEquals(60, retrieved.first().minutes)
            }

            @Test
            void 'Return 403 on attempted update by demo user'() {
                String jwt = getJwt('demo@demo.com', 'password')
                MockHttpServletRequestBuilder request = put('/api/exercise/performed').
                        contentType(MediaType.APPLICATION_JSON).
                        content(/{"exerciseId":"${UUID.randomUUID()}", "date":"${LocalDate.now()}", "minutes":30}/).
                        header(HttpHeaders.AUTHORIZATION, "local_$jwt")
                mockMvc.perform(request).
                        andExpect(status().is(HttpStatus.FORBIDDEN.value()))
            }

            @Test
            void 'Returns 200 on successful delete'() {
                Exercise testExercise = exerciseService.findByDescriptionLike('unicycling').first()
                ExercisePerformed exercisePerformed = new ExercisePerformed(userId: testUser.id, exerciseId: testExercise.id, date: LocalDate.now(testUser.timeZone), minutes: 30)
                exerciseService.upsertPerformed(exercisePerformed)

                MockHttpServletRequestBuilder request = delete("/api/exercise/performed/${exercisePerformed.id}").
                        header(HttpHeaders.AUTHORIZATION, "local_$testJwt")
                mockMvc.perform(request).
                        andExpect(status().isOk())

                assertEquals([], exerciseService.findByUserOnDate(testUser.id, LocalDate.now()))
            }

            @Test
            void 'Return 403 on attempted delete by demo user'() {
                String jwt = getJwt('demo@demo.com', 'password')
                MockHttpServletRequestBuilder request = delete("/api/exercise/performed/${UUID.randomUUID()}").
                        header(HttpHeaders.AUTHORIZATION, "local_$jwt")
                mockMvc.perform(request).
                        andExpect(status().is(HttpStatus.FORBIDDEN.value()))
            }

            @Test
            void 'Returns 403 on attempt to create for another user'() {
                Exercise testExercise = exerciseService.findByDescriptionLike('unicycling').first()

                MockHttpServletRequestBuilder request = post('/api/exercise/performed').
                        contentType(MediaType.APPLICATION_JSON).
                        content(/{"exerciseId":"${testExercise.id}", "userId":"${UUID.randomUUID()}", "date":"${LocalDate.now()}", "minutes":30}/).
                        header(HttpHeaders.AUTHORIZATION, "local_$testJwt")
                mockMvc.perform(request).
                        andExpect(status().is(HttpStatus.FORBIDDEN.value()))
            }

        }
    }

    @Nested
    @DisplayName('Food Controller')
    class FoodControllerTests {

        @Nested
        @DisplayName('Food')
        class FoodTests {

            @Test
            void 'Returns 200 and foods on successful search'() {
                MockHttpServletRequestBuilder request = get('/api/food/fish').
                        header(HttpHeaders.AUTHORIZATION, "local_$testJwt")
                String payload = mockMvc.perform(request).
                        andExpect(status().isOk()).
                        andReturn().response.contentAsString
                List<Map> foods = new JsonSlurper().parseText(payload) as List<Map>
                assertEquals(4, foods.size())
            }

            @Test
            void 'Returns 200 and recently eaten foods'() {
                List<Food> globalFoods = foodService.searchFoods(testUser.id, 'fish')
                FoodEaten eatenLastWeek = new FoodEaten(UUID.randomUUID(), testUser.id, globalFoods[0].id,
                        LocalDate.now(testUser.timeZone) - 6, globalFoods[0].defaultServingType, globalFoods[0].servingTypeQty)
                FoodEaten eatenLastMonth = new FoodEaten(UUID.randomUUID(), testUser.id, globalFoods[1].id,
                        LocalDate.now(testUser.timeZone) - 29, globalFoods[0].defaultServingType, globalFoods[1].servingTypeQty)
                foodService.upsertFoodEaten(eatenLastWeek)
                foodService.upsertFoodEaten(eatenLastMonth)

                MockHttpServletRequestBuilder request = get("/api/food/recent/${LocalDate.now() - 13}").
                        header(HttpHeaders.AUTHORIZATION, "local_$testJwt")
                String payload = mockMvc.perform(request).
                        andExpect(status().isOk()).
                        andReturn().response.contentAsString
                List<Map> foods = new JsonSlurper().parseText(payload) as List<Map>
                assertEquals(1, foods.size())
                assertEquals(eatenLastWeek.foodId.toString(), foods[0].id)
            }

            @Test
            void 'Returns 401 on unauthenticated request'() {
                MockHttpServletRequestBuilder request = get('/api/food/fish')
                mockMvc.perform(request).
                        andExpect(status().is(HttpStatus.UNAUTHORIZED.value()))
            }

            @Test
            void 'Returns 404 on no foods matching search'() {
                MockHttpServletRequestBuilder request = get('/api/food/no-matching-foods').
                        header(HttpHeaders.AUTHORIZATION, "local_$testJwt")
                mockMvc.perform(request).
                        andExpect(status().is(HttpStatus.NOT_FOUND.value()))
            }

            @Test
            void 'Returns 200 on successful food creation and update'() {
                UUID id = UUID.randomUUID()
                String createJson = testFoodJson(id, testUser.id, 50)

                MockHttpServletRequestBuilder createRequest = post('/api/food').
                        contentType(MediaType.APPLICATION_JSON).
                        content(createJson).
                        header(HttpHeaders.AUTHORIZATION, "local_$testJwt")
                String content = mockMvc.perform(createRequest).
                        andExpect(status().isOk()).
                        andReturn().response.contentAsString
                Map payload = new JsonSlurper().parseText(content) as Map

                assertEquals(id.toString(), payload.id)

                String updateJson = testFoodJson(id, testUser.id, 100)
                MockHttpServletRequestBuilder updateRequest = put('/api/food').
                        contentType(MediaType.APPLICATION_JSON).
                        content(updateJson).
                        header(HttpHeaders.AUTHORIZATION, "local_$testJwt")
                mockMvc.perform(updateRequest).
                        andExpect(status().isOk())
            }

            @Test
            void 'Returns 403 on attempted create by demo user'() {
                String jwt = getJwt('demo@demo.com', 'password')
                MockHttpServletRequestBuilder createRequest = post('/api/food').
                        contentType(MediaType.APPLICATION_JSON).
                        content(testFoodJson(UUID.randomUUID(), UUID.randomUUID(), 100)).
                        header(HttpHeaders.AUTHORIZATION, "local_$jwt")
                mockMvc.perform(createRequest).
                        andExpect(status().is(HttpStatus.FORBIDDEN.value()))
            }

            @Test
            void 'Returns 403 on attempted update by demo user'() {
                String jwt = getJwt('demo@demo.com', 'password')
                MockHttpServletRequestBuilder createRequest = put('/api/food').
                        contentType(MediaType.APPLICATION_JSON).
                        content(testFoodJson(UUID.randomUUID(), UUID.randomUUID(), 100)).
                        header(HttpHeaders.AUTHORIZATION, "local_$jwt")
                mockMvc.perform(createRequest).
                        andExpect(status().is(HttpStatus.FORBIDDEN.value()))
            }

            @Test
            void "Returns 200 and sets ID's for global food copy"() {
                Food globalFood = foodService.searchFoods(testUser.id, 'fish').first()
                UUID originalId = globalFood.id
                globalFood.calories = 100
                String customFoodJson = new ObjectMapper().writeValueAsString(globalFood)

                MockHttpServletRequestBuilder request = post('/api/food').
                        contentType(MediaType.APPLICATION_JSON).
                        content(customFoodJson).
                        header(HttpHeaders.AUTHORIZATION, "local_$testJwt")
                mockMvc.perform(request).
                        andExpect(status().isOk())

                Food customFood = foodService.searchFoods(testUser.id, 'fish').first()
                assertNotEquals(originalId, customFood.id)
                assertEquals(testUser.id, customFood.ownerId)
            }

            @Test
            void "Returns 403 on modifying other user's food"() {
                String createJson = testFoodJson(UUID.randomUUID(), UUID.randomUUID(), 100)

                MockHttpServletRequestBuilder createRequest = post('/api/food').
                        contentType(MediaType.APPLICATION_JSON).
                        content(createJson).
                        header(HttpHeaders.AUTHORIZATION, "local_$testJwt")
                mockMvc.perform(createRequest).
                        andExpect(status().is(HttpStatus.FORBIDDEN.value()))
            }

            @CompileDynamic
            private String testFoodJson(UUID i, UUID owner, int cal) {
                """
                {
                    "id":"$i",
                    "ownerId":"$owner",
                    "name":"Test Food",
                    "defaultServingType":"$Food.ServingType.OUNCE",
                    "servingTypeQty":1,
                    "calories":$cal,
                    "fat":1,
                    "saturatedFat":1,
                    "carbs":1,
                    "fiber":1,
                    "sugar":1,
                    "protein":1,
                    "sodium":1
                }
                """
            }

        }

        @Nested
        @DisplayName('Food Eaten')
        class FoodEatenTests {

            @Test
            void 'Returns 200 and foods eaten'() {
                Food testFood = foodService.searchFoods(testUser.id, 'fish').first()
                FoodEaten foodEaten = new FoodEaten(UUID.randomUUID(), testUser.id, testFood.id, LocalDate.now(testUser.timeZone), testFood.defaultServingType, testFood.servingTypeQty)
                foodService.upsertFoodEaten(foodEaten)

                MockHttpServletRequestBuilder request = get("/api/food/eaten/${LocalDate.now(testUser.timeZone)}").
                        header(HttpHeaders.AUTHORIZATION, "local_$testJwt")
                String payload = mockMvc.perform(request).
                        andExpect(status().isOk()).
                        andReturn().response.contentAsString
                List<Map> foodsEaten = new JsonSlurper().parseText(payload) as List<Map>
                assertEquals(1, foodsEaten.size())
            }

            @Test
            void 'Returns 400 for food eaten unparseable date'() {
                MockHttpServletRequestBuilder request = get('/api/food/eaten/this-is-not-a-date').
                        header(HttpHeaders.AUTHORIZATION, "local_$testJwt")
                mockMvc.perform(request).
                        andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
            }

            @Test
            void 'Returns 404 for no food eaten records found'() {
                MockHttpServletRequestBuilder request = get("/api/food/eaten/${LocalDate.now()}").
                        header(HttpHeaders.AUTHORIZATION, "local_$testJwt")
                mockMvc.perform(request).
                        andExpect(status().is(HttpStatus.NOT_FOUND.value()))
            }

            @Test
            void 'Returns 200 on successful create'() {
                Food testFood = foodService.searchFoods(testUser.id, 'fish').first()

                MockHttpServletRequestBuilder request = post('/api/food/eaten').
                        contentType(MediaType.APPLICATION_JSON).
                        content(/{"foodId":"${testFood.id}", "date":"${LocalDate.now(testUser.timeZone)}", "servingType":"${testFood.defaultServingType}", "servingQty":123}/).
                        header(HttpHeaders.AUTHORIZATION, "local_$testJwt")
                String content = mockMvc.perform(request).
                        andExpect(status().isOk()).
                        andReturn().response.contentAsString
                Map payload = new JsonSlurper().parseText(content) as Map

                List<FoodEaten> retrieved = foodService.findEaten(testUser.id, LocalDate.now(testUser.timeZone))
                assertEquals(1, retrieved.size())
                assertEquals(123, retrieved.first().servingQty)
                assertEquals(retrieved.first().id.toString(), payload.id)
            }

            @Test
            void 'Returns 403 on attempted create by demo user'() {
                String jwt = getJwt('demo@demo.com', 'password')
                MockHttpServletRequestBuilder request = post('/api/food/eaten').
                        contentType(MediaType.APPLICATION_JSON).
                        content(/{}/).
                        header(HttpHeaders.AUTHORIZATION, "local_$jwt")
                mockMvc.perform(request).
                        andExpect(status().is(HttpStatus.FORBIDDEN.value()))
            }

            @Test
            void 'Returns 200 on successful update'() {
                Food testFood = foodService.searchFoods(testUser.id, 'fish').first()
                FoodEaten foodEaten = new FoodEaten(UUID.randomUUID(), testUser.id, testFood.id, LocalDate.now(testUser.timeZone), testFood.defaultServingType, testFood.servingTypeQty)
                foodService.upsertFoodEaten(foodEaten)

                MockHttpServletRequestBuilder request = put('/api/food/eaten').
                        contentType(MediaType.APPLICATION_JSON).
                        content(/{"foodId":"${testFood.id}", "date":"${LocalDate.now(testUser.timeZone)}", "servingType":"${testFood.defaultServingType}", "servingQty":456}/).
                        header(HttpHeaders.AUTHORIZATION, "local_$testJwt")
                mockMvc.perform(request).
                        andExpect(status().isOk())

                List<FoodEaten> retrieved = foodService.findEaten(testUser.id, LocalDate.now(testUser.timeZone))
                assertEquals(1, retrieved.size())
                assertEquals(456, retrieved.first().servingQty)
            }

            @Test
            void 'Returns 403 on attempt to create for another user'() {
                Food testFood = foodService.searchFoods(testUser.id, 'fish').first()

                MockHttpServletRequestBuilder request = post('/api/food/eaten').
                        contentType(MediaType.APPLICATION_JSON).
                        content(/{"foodId":"${testFood.id}", "userId":"${UUID.randomUUID()}", "date":"${LocalDate.now(testUser.timeZone)}", "servingType":"${testFood.defaultServingType}", "servingQty":123}/).
                        header(HttpHeaders.AUTHORIZATION, "local_$testJwt")
                mockMvc.perform(request).
                        andExpect(status().is(HttpStatus.FORBIDDEN.value()))
            }

            @Test
            void 'Returns 403 on attempted update by demo user'() {
                String jwt = getJwt('demo@demo.com', 'password')
                MockHttpServletRequestBuilder request = put('/api/food/eaten').
                        contentType(MediaType.APPLICATION_JSON).
                        content(/{}/).
                        header(HttpHeaders.AUTHORIZATION, "local_$jwt")
                mockMvc.perform(request).
                        andExpect(status().is(HttpStatus.FORBIDDEN.value()))
            }

            @Test
            void 'Returns 200 on successful delete'() {
                Food testFood = foodService.searchFoods(testUser.id, 'fish').first()
                FoodEaten foodEaten = new FoodEaten(UUID.randomUUID(), testUser.id, testFood.id, LocalDate.now(testUser.timeZone), testFood.defaultServingType, testFood.servingTypeQty)
                foodService.upsertFoodEaten(foodEaten)

                MockHttpServletRequestBuilder request = delete("/api/food/eaten/${foodEaten.id}").
                        header(HttpHeaders.AUTHORIZATION, "local_$testJwt")
                mockMvc.perform(request).
                        andExpect(status().isOk())

                assertEquals([], foodService.findEaten(testUser.id, LocalDate.now(testUser.timeZone)))
            }

            @Test
            void 'Returns 403 on attempted delete by demo user'() {
                String jwt = getJwt('demo@demo.com', 'password')
                MockHttpServletRequestBuilder request = delete("/api/food/eaten/${UUID.randomUUID()}").
                        contentType(MediaType.APPLICATION_JSON).
                        content(/{}/).
                        header(HttpHeaders.AUTHORIZATION, "local_$jwt")
                mockMvc.perform(request).
                        andExpect(status().is(HttpStatus.FORBIDDEN.value()))
            }

        }

    }

    @Nested
    @DisplayName('User Controller')
    class UserControllerTests {

        @Nested
        @DisplayName('Authentication')
        class AuthTests {

            @Test
            void 'Returns JWT on success'() {
                MockHttpServletRequestBuilder request = post('/api/user/auth').
                        contentType(MediaType.APPLICATION_JSON).
                        content('{"email":"demo@demo.com", "password":"password"}')
                mockMvc.perform(request).
                        andExpect(status().isOk()).
                        andExpect(content().string(containsString('{"token":"')))
            }

            @Test
            void 'Returns 400 on missing inputs'() {
                MockHttpServletRequestBuilder request = post('/api/user/auth').
                        contentType(MediaType.APPLICATION_JSON)
                mockMvc.perform(request).
                        andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
            }

            @Test
            void 'Returns 401 on unrecognized email'() {
                MockHttpServletRequestBuilder request = post('/api/user/auth').
                        contentType(MediaType.APPLICATION_JSON).
                        content('{"email":"unknown@unknown.com", "password":"no such user anyway"}')
                mockMvc.perform(request).
                        andExpect(status().is(HttpStatus.UNAUTHORIZED.value())).
                        andExpect(content().json('{"error":"The username and password are not valid"}'))
            }

            @Test
            void 'Returns 401 on wrong password'() {
                MockHttpServletRequestBuilder request = post('/api/user/auth').
                        contentType(MediaType.APPLICATION_JSON).
                        content('{"email":"demo@demo.com", "password":"wrong password!"}')
                mockMvc.perform(request).
                        andExpect(status().is(HttpStatus.UNAUTHORIZED.value())).
                        andExpect(content().json('{"error":"The username and password are not valid"}'))
            }

        }

        @Nested
        @DisplayName('Load user')
        class LoadUserTests {

            @Test
            void 'Returns user JSON on successful lookup'() {
                MockHttpServletRequestBuilder request = get('/api/user').
                        header(HttpHeaders.AUTHORIZATION, "local_$testJwt")
                mockMvc.perform(request).
                        andExpect(status().isOk()).
                        andExpect(content().string('{"gender":"MALE","birthdate":"1985-01-01","heightInInches":70.0,"activityLevel":"SEDENTARY","email":"test@test.com","passwordHash":"$2a$10$y9lTADvs0HW5XGOj41sXQOWlRqb3e20e28TruX..MtXD8zfOeIH.e","firstName":"Test","lastName":"User","timeZone":"America/New_York"}'))
            }

            @Test
            void 'Returns 401 on missing email attribute'() {
                MockHttpServletRequestBuilder request = get('/api/user')
                mockMvc.perform(request).
                        andExpect(status().is(HttpStatus.UNAUTHORIZED.value()))
            }

            @Test
            void 'Returns 401 on unknown user'() {
                MockHttpServletRequestBuilder request = get('/api/user').
                        requestAttr('email', 'unknown@unknown.com')
                mockMvc.perform(request).
                        andExpect(status().is(HttpStatus.UNAUTHORIZED.value()))
            }

        }

        @Nested
        @DisplayName('Create user')
        class CreateUserTests {

            @Test
            void 'Returns 200 on success'() {
                MockHttpServletRequestBuilder request = post('/api/user').
                        contentType(MediaType.APPLICATION_JSON).
                        content(/{"email":"new@new.com", "password":"password"}/)
                mockMvc.perform(request).
                        andExpect(status().isOk())
            }

            @Test
            void 'Returns 500 on duplicate email'() {
                // This test should cause an exception down in the DAO layer to bubble up through the controller layer,
                // returning a 500 status code to the client.  Unfortunately, limitations in MockMvc prevent us from
                // testing this precisely:  https://github.com/spring-projects/spring-boot/issues/7321.  However, returning
                // a 500 when an exception bubbles up is framework-level Spring functionality.
                Exception e = null
                try {
                    MockHttpServletRequestBuilder request = post('/api/user').
                            contentType(MediaType.APPLICATION_JSON).
                            content(/{"email":"demo@demo.com", "password":"password"}/)
                    mockMvc.perform(request).
                            andExpect(status().is5xxServerError())
                } catch (Exception ee) {
                    e = ee
                }
                assertTrue e instanceof NestedServletException
                assertTrue e.cause instanceof SQLIntegrityConstraintViolationException
                assertEquals("Duplicate entry 'demo@demo.com' for key 'email_UNIQUE'", e.cause.message)
            }

        }

        @Nested
        @DisplayName('Update user')
        class UpdateUserTests {

            @Test
            void 'Returns 200 on success'() {
                MockHttpServletRequestBuilder request = put('/api/user').
                        header(HttpHeaders.AUTHORIZATION, "local_$testJwt").
                        contentType(MediaType.APPLICATION_JSON).
                        content(/{"gender":"MALE","birthdate":"1990-01-01","heightInInches":70,"activityLevel":"EXTREMELY_ACTIVE","email":"updated@updated.com","firstName":"New","lastName":"Name","timeZone":"UTC"}/)
                mockMvc.perform(request).
                        andExpect(status().isOk())
            }

            @Test
            void 'Returns 400 on missing payload'() {
                MockHttpServletRequestBuilder request = put('/api/user').
                        header(HttpHeaders.AUTHORIZATION, "local_$testJwt").
                        contentType(MediaType.APPLICATION_JSON)
                mockMvc.perform(request).
                        andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
            }

            @Test
            void 'Returns 401 on missing JWT'() {
                MockHttpServletRequestBuilder request = put('/api/user').
                        contentType(MediaType.APPLICATION_JSON).
                        content(/{"gender":"MALE","birthdate":"1990-01-01","heightInInches":70,"activityLevel":"EXTREMELY_ACTIVE","email":"updated@updated.com","firstName":"New","lastName":"Name","timeZone":"UTC"}/)
                mockMvc.perform(request).
                        andExpect(status().is(HttpStatus.UNAUTHORIZED.value()))
            }

            @Test
            void 'Returns 403 on attempted change by demo user'() {
                String jwt = getJwt('demo@demo.com', 'password')
                MockHttpServletRequestBuilder request = put('/api/user').
                        header(HttpHeaders.AUTHORIZATION, "local_$jwt").
                        contentType(MediaType.APPLICATION_JSON).
                        content(/{}/)
                mockMvc.perform(request).
                        andExpect(status().is(HttpStatus.FORBIDDEN.value()))
            }

        }

        @Nested
        @DisplayName('Change password')
        class ChangePasswordTests {

            @Test
            void 'Returns 200 and new JWT on success'() {
                MockHttpServletRequestBuilder request = put('/api/user/password').
                        header(HttpHeaders.AUTHORIZATION, "local_$testJwt").
                        contentType(MediaType.APPLICATION_JSON).
                        content(/{"currentPassword":"password","newPassword":"newPassword","confirmNewPassword":"newPassword"}/)
                mockMvc.perform(request).
                        andExpect(status().isOk()).
                        andExpect(content().string(containsString('{"token":"')))
            }

            @Test
            void 'Returns 400 on failure'() {
                MockHttpServletRequestBuilder request = put('/api/user/password').
                        header(HttpHeaders.AUTHORIZATION, "local_$testJwt").
                        contentType(MediaType.APPLICATION_JSON).
                        content(/{"currentPassword":"wrong-password","newPassword":"newPassword","confirmNewPassword":"newPassword"}/)
                mockMvc.perform(request).
                        andExpect(status().is(HttpStatus.BAD_REQUEST.value())).
                        andExpect(content().json('{"error":"The current password is not correct"}'))
            }

            @Test
            void 'Returns 403 on attempted change by demo user'() {
                String jwt = getJwt('demo@demo.com', 'password')
                MockHttpServletRequestBuilder request = put('/api/user/password').
                        header(HttpHeaders.AUTHORIZATION, "local_$jwt").
                        contentType(MediaType.APPLICATION_JSON).
                        content(/{}/)
                mockMvc.perform(request).
                        andExpect(status().is(HttpStatus.FORBIDDEN.value()))
            }

        }

    }

    @Nested
    @DisplayName('Weight Controller')
    class WeightControllerTests {

        @Nested
        @DisplayName('Load weight')
        class LoadWeightTests {

            @Test
            void 'Returns 200 on success'() {
                weightService.upsert(new Weight(userId: testUser.id, date: LocalDate.now(testUser.timeZone), pounds: 150))

                MockHttpServletRequestBuilder request = get("/api/weight/${LocalDate.now(testUser.timeZone).toString()}").
                        header(HttpHeaders.AUTHORIZATION, "local_$testJwt")
                mockMvc.perform(request).
                        andExpect(status().isOk()).
                        andExpect(content().string('150.0'))
            }

            @Test
            void 'Returns 401 on unauthorized request'() {
                MockHttpServletRequestBuilder request = get("/api/weight/${LocalDate.now().toString()}")
                mockMvc.perform(request).
                        andExpect(status().is(HttpStatus.UNAUTHORIZED.value()))
            }

            @Test
            void 'Returns 404 on no records found'() {
                MockHttpServletRequestBuilder request = get("/api/weight/${LocalDate.now().toString()}").
                        header(HttpHeaders.AUTHORIZATION, "local_$testJwt")
                mockMvc.perform(request).
                        andExpect(status().is(HttpStatus.NOT_FOUND.value()))
            }

        }

        @Nested
        @DisplayName('Upsert weight')
        class UpsertWeightTests {

            @Test
            void 'Returns 200 on successful insert'() {
                MockHttpServletRequestBuilder request = post("/api/weight/${LocalDate.now()}").
                        header(HttpHeaders.AUTHORIZATION, "local_$testJwt").
                        contentType(MediaType.APPLICATION_JSON).
                        content(/{"id":"${UUID.randomUUID()}", "userId":"${testUser.id}", "date":"${LocalDate.now().toString()}", "pounds":160}/)
                mockMvc.perform(request).
                        andExpect(status().isOk())
            }

            @Test
            void 'Returns 403 on attempted insert by demo user'() {
                String jwt = getJwt('demo@demo.com', 'password')
                MockHttpServletRequestBuilder request = post("/api/weight/${LocalDate.now()}").
                        header(HttpHeaders.AUTHORIZATION, "local_$jwt").
                        contentType(MediaType.APPLICATION_JSON).
                        content(/{}/)
                mockMvc.perform(request).
                        andExpect(status().is(HttpStatus.FORBIDDEN.value()))
            }

            @Test
            void 'Returns 200 on successful update'() {
                weightService.upsert(new Weight(userId: testUser.id, date: LocalDate.now(testUser.timeZone), pounds: 150))

                MockHttpServletRequestBuilder request = put("/api/weight/${LocalDate.now(testUser.timeZone).toString()}").
                        header(HttpHeaders.AUTHORIZATION, "local_$testJwt").
                        contentType(MediaType.APPLICATION_JSON).
                        content(/{"userId":"${testUser.id}", "date":"${LocalDate.now(testUser.timeZone).toString()}", "pounds":160}/)
                mockMvc.perform(request).
                        andExpect(status().isOk())
            }

            @Test
            void 'Returns 403 on attempted update by demo user'() {
                String jwt = getJwt('demo@demo.com', 'password')
                MockHttpServletRequestBuilder request = put("/api/weight/${LocalDate.now()}").
                        header(HttpHeaders.AUTHORIZATION, "local_$jwt").
                        contentType(MediaType.APPLICATION_JSON).
                        content(/{}/)
                mockMvc.perform(request).
                        andExpect(status().is(HttpStatus.FORBIDDEN.value()))
            }

        }

    }

}
