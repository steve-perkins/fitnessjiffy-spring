package com.steveperkins.fitnessjiffy.service

import com.steveperkins.fitnessjiffy.dao.FoodDAO
import com.steveperkins.fitnessjiffy.domain.Food
import com.steveperkins.fitnessjiffy.domain.FoodEaten
import groovy.transform.CompileStatic
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.junit.jupiter.MockitoExtension

import java.time.LocalDate

import static org.junit.jupiter.api.Assertions.assertEquals
import static org.mockito.Mockito.verify
import static org.mockito.Mockito.when

@ExtendWith(MockitoExtension.class)
@CompileStatic
class FoodServiceTests {

    @Mock
    UserService userService

    @Mock
    FoodDAO foodDAO

    @InjectMocks
    FoodService foodService

    @Test
    void 'Search foods'() {
        UUID id = UUID.randomUUID()
        String searchString = 'fish'

        foodService.searchFoods(id, searchString)

        verify(foodDAO).searchFoods(id, searchString)
    }

    @Test
    void 'Find foods eaten within date range'() {
        UUID userId = UUID.randomUUID()

        foodService.findByUserPerformedWithinRange(userId, LocalDate.now() - 13, LocalDate.now())

        verify(foodDAO).findByUserPerformedWithinRange(userId, LocalDate.now() - 13, LocalDate.now())
    }

    @Test
    void 'Upsert food'() {
        Food testFood = new Food(id: UUID.randomUUID(), ownerId: UUID.randomUUID())
        when(foodDAO.findEarliestFoodEaten(testFood.ownerId, testFood.id)).
                thenReturn(Optional.of(new FoodEaten(date: LocalDate.now())))

        foodService.upsertFood(testFood)

        verify(foodDAO).upsertFood(testFood)
        verify(foodDAO).findEarliestFoodEaten(testFood.ownerId, testFood.id)
        verify(userService).updateReportsDirtyDate(testFood.ownerId, LocalDate.now())
    }

    @Test
    void 'Find eaten, no serving type conversion'() {
        UUID userId = UUID.randomUUID()
        Food testFood = new Food(UUID.randomUUID(), userId, 'Test Food', Food.ServingType.OUNCE, 1, 1, 1, 1, 1, 1, 1, 1, 1)
        FoodEaten foodEaten = new FoodEaten(UUID.randomUUID(), userId, testFood.id, LocalDate.now(), testFood.defaultServingType, testFood.servingTypeQty * 2)

        when(foodDAO.findEatenOnDate(userId, LocalDate.now())).thenReturn([foodEaten])
        when(foodDAO.findFoodById(testFood.id)).thenReturn(Optional.of(testFood))

        List<FoodEaten> retrieved = foodService.findEaten(userId, LocalDate.now())
        assertEquals(1, retrieved.size())
        // Two servings were eaten, so all of the food record stats (i.e. all 1's) should be doubled.
        assertEquals(2, retrieved.first().servingQty)
        assertEquals(2, retrieved.first().calories)
        assertEquals(2, retrieved.first().fat)
        assertEquals(2, retrieved.first().saturatedFat)
        assertEquals(2, retrieved.first().carbs)
        assertEquals(2, retrieved.first().fiber)
        assertEquals(2, retrieved.first().sugar)
        assertEquals(2, retrieved.first().protein)
        assertEquals(2, retrieved.first().sodium)
    }

    @Test
    void 'Find eaten, with serving type conversion'() {
        UUID userId = UUID.randomUUID()
        Food testFood = new Food(UUID.randomUUID(), userId, 'Test Food', Food.ServingType.OUNCE, 1, 1, 1, 1, 1, 1, 1, 1, 1)
        FoodEaten foodEaten = new FoodEaten(UUID.randomUUID(), userId, testFood.id, LocalDate.now(), Food.ServingType.CUP, testFood.servingTypeQty)

        when(foodDAO.findEatenOnDate(userId, LocalDate.now())).thenReturn([foodEaten])
        when(foodDAO.findFoodById(testFood.id)).thenReturn(Optional.of(testFood))

        List<FoodEaten> retrieved = foodService.findEaten(userId, LocalDate.now())
        assertEquals(1, retrieved.size())
        assertEquals(1, retrieved.first().servingQty)
        // The regular serving size is one ounce, but one cup (i.e. 8 ounces) was eaten.  All of the food record stats (i.e. all 1's) should be 8x.
        assertEquals(8, retrieved.first().calories)
        assertEquals(8, retrieved.first().fat)
        assertEquals(8, retrieved.first().saturatedFat)
        assertEquals(8, retrieved.first().carbs)
        assertEquals(8, retrieved.first().fiber)
        assertEquals(8, retrieved.first().sugar)
        assertEquals(8, retrieved.first().protein)
        assertEquals(8, retrieved.first().sodium)
    }

    @Test
    void 'Upsert food eaten'() {
        FoodEaten foodEaten = new FoodEaten(UUID.randomUUID(), UUID.randomUUID(), UUID.randomUUID(), LocalDate.now(), Food.ServingType.OUNCE, 1)

        foodService.upsertFoodEaten(foodEaten)

        verify(foodDAO).upsertFoodEaten(foodEaten)
        verify(userService).updateReportsDirtyDate(foodEaten.userId, foodEaten.date)
    }

    @Test
    void 'Delete food eaten'() {
        FoodEaten foodEaten = new FoodEaten(id: UUID.randomUUID(), userId: UUID.randomUUID(), date: LocalDate.now())
        when(foodDAO.findEatenById(foodEaten.id)).thenReturn(Optional.of(foodEaten))

        foodService.deleteFoodEaten(foodEaten.id)

        verify(foodDAO).deleteFoodEaten(foodEaten.id)
        verify(userService).updateReportsDirtyDate(foodEaten.userId, foodEaten.date)
    }

}
