package com.steveperkins.fitnessjiffy.service

import com.steveperkins.fitnessjiffy.dao.WeightDAO
import com.steveperkins.fitnessjiffy.domain.Weight
import groovy.transform.CompileStatic
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.junit.jupiter.MockitoExtension

import java.time.LocalDate

import static org.mockito.Mockito.verify

@ExtendWith(MockitoExtension.class)
@CompileStatic
class WeightServiceTests {

    @Mock
    UserService userService

    @Mock
    WeightDAO weightDAO

    @InjectMocks
    WeightService weightService

    @Test
    void 'Find most recent on date'() {
        UUID userId = UUID.randomUUID()
        weightService.findWeightOnDate(userId, LocalDate.now())

        verify(weightDAO).findByUserMostRecentOnDate(userId, LocalDate.now())
    }

    @Test
    void 'Upsert'() {
        Weight weight = new Weight(userId: UUID.randomUUID(), date: LocalDate.now())

        weightService.upsert(weight)

        verify(weightDAO).upsert(weight)
        verify(userService).updateReportsDirtyDate(weight.userId, weight.date)
    }

}
