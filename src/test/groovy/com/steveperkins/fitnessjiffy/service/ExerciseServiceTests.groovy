package com.steveperkins.fitnessjiffy.service

import com.steveperkins.fitnessjiffy.dao.ExerciseDAO
import com.steveperkins.fitnessjiffy.dao.WeightDAO
import com.steveperkins.fitnessjiffy.domain.Exercise
import com.steveperkins.fitnessjiffy.domain.ExercisePerformed
import com.steveperkins.fitnessjiffy.domain.Weight
import groovy.transform.CompileStatic
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.junit.jupiter.MockitoExtension

import java.time.LocalDate

import static org.junit.jupiter.api.Assertions.assertEquals
import static org.mockito.Mockito.verify
import static org.mockito.Mockito.when

@ExtendWith(MockitoExtension.class)
@CompileStatic
class ExerciseServiceTests {

    @Mock
    UserService userService

    @Mock
    ExerciseDAO exerciseDAO

    @Mock
    WeightDAO weightDAO

    @InjectMocks
    ExerciseService exerciseService

    //
    // Exercise
    //

    @Test
    void 'Find exercise by description'() {
        exerciseService.findByDescriptionLike('test')

        verify(exerciseDAO).findByDescriptionLike('test')
    }

    @Test
    void 'Find all categories'() {
        exerciseService.findAllCategories()

        verify(exerciseDAO).findAllCategories()
    }

    @Test
    void 'Find by category'() {
        exerciseService.findByCategory('mock')

        verify(exerciseDAO).findByCategory('mock')
    }

    @Test
    void 'Find by user performed within date range'() {
        UUID userId = UUID.randomUUID()
        LocalDate today = LocalDate.now()
        LocalDate lastWeek = today - 7
        exerciseService.findByUserPerformedWithinRange(userId, lastWeek, today)

        verify(exerciseDAO).findByUserPerformedWithinRange(userId, lastWeek, today)
    }

    //
    // Exercise Performed
    //

    @Test
    void 'Upsert exercise performed record'() {
        UUID userId = UUID.randomUUID()
        LocalDate date = LocalDate.now()
        ExercisePerformed exercisePerformed = new ExercisePerformed(UUID.randomUUID(), userId, UUID.randomUUID(), date, 30)

        exerciseService.upsertPerformed(exercisePerformed)

        verify(exerciseDAO).upsertPerformed(exercisePerformed)
        verify(userService).updateReportsDirtyDate(userId, date)
    }

    @Test
    void 'Delete exercise performed record'() {
        ExercisePerformed exercisePerformed = new ExercisePerformed(UUID.randomUUID(), UUID.randomUUID(), UUID.randomUUID(), LocalDate.now())
        when(exerciseDAO.findPerformedById(exercisePerformed.id)).thenReturn(Optional.of(exercisePerformed))

        exerciseService.deletePerformed(exercisePerformed.id)

        verify(exerciseDAO).deletePerformed(exercisePerformed.id)
        verify(userService).updateReportsDirtyDate(exercisePerformed.userId, exercisePerformed.date)
    }

    @Test
    void 'Find by user on date succeeds'() {
        LocalDate date = LocalDate.now()
        UUID userId = UUID.randomUUID()
        UUID exerciseId = UUID.randomUUID()

        when(exerciseDAO.findPerformedByUserAndDate(userId, date)).thenReturn([new ExercisePerformed(UUID.randomUUID(), userId, exerciseId, date, 30)])
        when(exerciseDAO.findById(exerciseId)).thenReturn(Optional.of(new Exercise(exerciseId, 'code', 2, 'category', 'Test Exercise')))
        when(weightDAO.findByUserMostRecentOnDate(userId, date)).thenReturn(new Weight(pounds: 150))

        List<ExercisePerformed> retrieved = exerciseService.findByUserOnDate(userId, date)

        assertEquals(1, retrieved.size())
        assertEquals(71, retrieved.first().calories)

        verify(exerciseDAO).findPerformedByUserAndDate(userId, date)
        verify(exerciseDAO).findById(exerciseId)
        verify(weightDAO).findByUserMostRecentOnDate(userId, date)
    }

    @Test
    void 'Find by user on date fails on unknown exercise'() {
        LocalDate date = LocalDate.now()
        UUID userId = UUID.randomUUID()
        UUID exerciseId = UUID.randomUUID()

        when(exerciseDAO.findPerformedByUserAndDate(userId, date)).thenReturn([new ExercisePerformed(UUID.randomUUID(), userId, exerciseId, date, 30)])
        when(exerciseDAO.findById(exerciseId)).thenReturn(Optional.empty())

        List<ExercisePerformed> retrieved = exerciseService.findByUserOnDate(userId, date)

        assertEquals(0, retrieved.size())

        verify(exerciseDAO).findPerformedByUserAndDate(userId, date)
        verify(exerciseDAO).findById(exerciseId)
    }

    @Test
    void 'Find by user on date fails on no weight records found'() {
        LocalDate date = LocalDate.now()
        UUID userId = UUID.randomUUID()
        UUID exerciseId = UUID.randomUUID()

        when(exerciseDAO.findPerformedByUserAndDate(userId, date)).thenReturn([new ExercisePerformed(UUID.randomUUID(), userId, exerciseId, date, 30)])
        when(exerciseDAO.findById(exerciseId)).thenReturn(Optional.of(new Exercise(exerciseId, 'code', 2, 'category', 'Test Exercise')))
        when(weightDAO.findByUserMostRecentOnDate(userId, date)).thenReturn(null)

        List<ExercisePerformed> retrieved = exerciseService.findByUserOnDate(userId, date)

        assertEquals(0, retrieved.size())

        verify(exerciseDAO).findPerformedByUserAndDate(userId, date)
        verify(exerciseDAO).findById(exerciseId)
        verify(weightDAO).findByUserMostRecentOnDate(userId, date)
    }

}
