package com.steveperkins.fitnessjiffy.service

import com.steveperkins.fitnessjiffy.dao.UserDAO
import com.steveperkins.fitnessjiffy.exception.AuthenticationException
import com.steveperkins.fitnessjiffy.domain.User
import groovy.transform.CompileStatic
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mindrot.jbcrypt.BCrypt
import org.mockito.ArgumentCaptor
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.junit.jupiter.MockitoExtension

import java.sql.Timestamp
import java.time.LocalDate
import java.time.ZoneId

import static org.junit.jupiter.api.Assertions.assertEquals
import static org.junit.jupiter.api.Assertions.assertFalse
import static org.junit.jupiter.api.Assertions.assertThrows
import static org.junit.jupiter.api.Assertions.assertTrue
import static org.mockito.ArgumentMatchers.any
import static org.mockito.ArgumentMatchers.anyString
import static org.mockito.ArgumentMatchers.eq
import static org.mockito.Mockito.mock
import static org.mockito.Mockito.times
import static org.mockito.Mockito.verify
import static org.mockito.Mockito.when

@ExtendWith(MockitoExtension.class)
@CompileStatic
class UserServiceTests {

    @Mock
    UserDAO userDAO

    @InjectMocks
    UserService userService

    @Test
    void 'Find user by email'() {
        when(userDAO.findByEmail('mock@mock.com')).thenReturn(Optional.of(new User()))

        Optional<User> found = userService.findByEmail('mock@mock.com')
        Optional<User> shouldBeNull = userService.findByEmail(null)
        Optional<User> noSuchUser = userService.findByEmail('unknown@unknown.com')

        assertTrue found.isPresent()
        assertFalse shouldBeNull.isPresent()
        assertFalse noSuchUser.isPresent()
        verify(userDAO).findByEmail('unknown@unknown.com')
        verify(userDAO).findByEmail('mock@mock.com')
    }

    @Test
    void 'Create new user'() {
        userService.createUser('mock@mock.com', 'password')

        ArgumentCaptor<User> argumentCaptor = ArgumentCaptor.forClass(User.class)
        verify(userDAO).create(argumentCaptor.capture())

        assertEquals('mock@mock.com', argumentCaptor.value.email)
        assertTrue BCrypt.checkpw('password', argumentCaptor.value.passwordHash)
    }

    @Test
    void 'Update user'() {
        UUID orginalId = UUID.randomUUID()
        Timestamp createdTime = new Timestamp(System.currentTimeMillis())
        User testUser = new User(id: orginalId, email: 'mock@mock.com', passwordHash: 'original-password-hash', createdTime: createdTime)
        when(userDAO.update(any(User.class)) as Object).thenAnswer { invocation ->
            // An updated with "null" values doesn't actually change anything
            User arg = invocation.arguments[0] as User
            assertEquals(User.Gender.FEMALE, arg.gender)
            assertEquals(testUser.birthdate, arg.birthdate)
            assertEquals(60, arg.heightInInches)
            assertEquals(User.ActivityLevel.SEDENTARY, arg.activityLevel)
            assertEquals('First Name', arg.firstName)
            assertEquals('Last Name', arg.lastName)
            assertEquals(ZoneId.of('America/New_York'), arg.timeZone)
            assertEquals('mock@mock.com', arg.email)
            assertEquals('original-password-hash', arg.passwordHash)
            return null
        }
        userService.updateUser(testUser, null, null, null, null, null, null, null, null)

        testUser.id = UUID.randomUUID()
        testUser.createdTime = new Timestamp(System.currentTimeMillis())
        when(userDAO.update(any(User.class)) as Object).thenAnswer { invocation ->
            // An update with non-null values affects the object
            User arg = invocation.arguments[0] as User
            assertEquals('updated@updated.com', arg.email)
            assertEquals('original-password-hash', arg.passwordHash)
            assertEquals(User.Gender.MALE, arg.gender)
            assertEquals(LocalDate.now() - 1, arg.birthdate)
            assertEquals(70, arg.heightInInches)
            assertEquals(User.ActivityLevel.EXTREMELY_ACTIVE, arg.activityLevel)
            assertEquals('Test', arg.firstName)
            assertEquals('User', arg.lastName)
            assertEquals(ZoneId.of('UTC'), arg.timeZone)
            return null
        }
        userService.updateUser(testUser, User.Gender.MALE, LocalDate.now() - 1, 70, User.ActivityLevel.EXTREMELY_ACTIVE, 'updated@updated.com', 'Test', 'User', ZoneId.of('UTC'))
    }

    @Test
    void 'User password verification'() {
        User testUser = new User(email: 'mock@mock.com', passwordHash: '$2a$10$y9lTADvs0HW5XGOj41sXQOWlRqb3e20e28TruX..MtXD8zfOeIH.e')  // plaintext == 'password'
        when(userDAO.findByEmail('mock@mock.com')).thenReturn(Optional.of(testUser))

        assertTrue userService.verifyPassword('mock@mock.com', 'password')
        assertFalse userService.verifyPassword('mock@mock.com', 'wrong password!')
        assertFalse userService.verifyPassword(null, "user ID is null, password won't be checked")
        assertFalse userService.verifyPassword('mock@mock.com', "email is unknown, password won't be checked")
        verify(userDAO, times(3)).findByEmail('mock@mock.com')
    }

    @Test
    void 'Change password succeeds'() {
        User testUser = new User(email: 'mock@mock.com', passwordHash: '$2a$10$y9lTADvs0HW5XGOj41sXQOWlRqb3e20e28TruX..MtXD8zfOeIH.e')
        when(userDAO.findByEmail('mock@mock.com')).thenReturn(Optional.of(testUser))

        userService.changePassword(testUser, 'password', 'newPassword', 'newPassword')
        verify(userDAO).changePassword(eq(testUser), anyString())
    }

    @Test
    void "Change password fails because the 'new password' and 'confirm new password' values don't match"() {
        assertThrows(AuthenticationException.class, {
            userService.changePassword(mock(User), 'currentPassword', 'newPassword', "doesn't match newPassword!")
        })
    }

    @Test
    void "Change password fails because the 'old password' value isn't correct"() {
        assertThrows(AuthenticationException.class, {
            when(userDAO.findByEmail('mock@mock.com')).
                    thenReturn(Optional.of(new User(email: 'mock@mock.com', passwordHash: '$2a$10$JnICd7OwpBxwofwQWFb9A.gXlZHWXQ15ZIpWbCk2bpH6fCn9Dr.tm')))  // plaintext: "wrong-password"

            userService.changePassword(new User(email: 'mock@mock.com'), 'currentPassword', 'newPassword', 'newPassword')
        })
    }

    @Test
    void 'Update reportsDirtyDate when current value is null'() {
        User user = new User(id: UUID.randomUUID(), reportsDirtyDate: null as LocalDate)
        when(userDAO.findById(user.id)).thenReturn(Optional.of(user))

        userService.updateReportsDirtyDate(user.id, LocalDate.now())

        verify(userDAO).update(user)
    }

    @Test
    void 'Update reportsDirtyDate when current value is more recent'() {
        User user = new User(id: UUID.randomUUID(), reportsDirtyDate: LocalDate.now())
        when(userDAO.findById(user.id)).thenReturn(Optional.of(user))

        userService.updateReportsDirtyDate(user.id, LocalDate.now() - 7)

        verify(userDAO).update(user)
    }

    @Test
    void 'Does not update reportsDirtyDate when current value is prior'() {
        User user = new User(id: UUID.randomUUID(), reportsDirtyDate: LocalDate.now() - 7)
        when(userDAO.findById(user.id)).thenReturn(Optional.of(user))

        userService.updateReportsDirtyDate(user.id, LocalDate.now())

        verify(userDAO, times(0)).update(user)
    }

}
